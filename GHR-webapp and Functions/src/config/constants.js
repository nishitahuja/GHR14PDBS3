import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyBcb0YCVNtP1kImjIqXJgFRc3ZP4qJksQE",
    authDomain: "goldenhourresponse-89938.firebaseapp.com",
    databaseURL: "https://goldenhourresponse-89938.firebaseio.com",
    projectId: "goldenhourresponse-89938",
    storageBucket: "goldenhourresponse-89938.appspot.com",
    messagingSenderId: "522181940940"
  };

firebase.initializeApp(config)

export const ref = firebase.database().ref()
export const firebaseAuth = firebase.auth