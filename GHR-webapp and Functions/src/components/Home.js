import React, { Component } from 'react'

export default class Home extends Component {
  render () {
    return (
      <div>
        Welcome to the admin console for the app Golden Hour Response!
      </div>
    )
  }
}
