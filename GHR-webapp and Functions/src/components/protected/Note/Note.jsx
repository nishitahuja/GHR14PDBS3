import React, { Component } from 'react';
import './Note.css';
import PropTypes from 'prop-types';

class Note extends Component{

    constructor(props){
        super(props);
        this.noteContent = props.noteContent,
        this.noteId = props.noteId,
        this.hname=props.hname,
        this.haddress=props.haddress,
        this.hno=props.hno,
        this.handleRemoveNote = this.handleRemoveNote.bind(this);
    }

    handleRemoveNote(id){
        this.props.removeNote(id);
          window.location.href='/dashboard';
    }

    render(){
        return(
            <div className="note fade-in">
                <span className="closebtn"
                      onClick={() => this.handleRemoveNote(this.noteId)}>
                      &times;
                </span>
                <h3>Hospital Name: { this.hname } Hospital Address: { this.haddress } Hospital No: { this.hno }</h3>
            </div>
        )
    }
}

Note.propTypes = {
    noteContent: PropTypes.string,
    hname: PropTypes.string,
    haddress: PropTypes.string,
    hno: PropTypes.string,
}

export default Note;
