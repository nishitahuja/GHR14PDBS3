import React, { Component } from 'react'
import { saveHospital } from '../../helpers/auth'
export default class AddHospital extends Component {
  state = { loginMessage: null }
    handleSubmit = (e) => {
      e.preventDefault()
      saveHospital(this.hospitalname.value, this.hospitalno.value,this.hospitaladdress.value,this.pincode.value)
    window.location.href='/dashboard';
    }
  render () {
    return (
      <div>
      <div className="col-sm-6 col-sm-offset-3">
        <h1 > Add Hospital </h1>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Hospital Name</label>
            <input className="form-control" ref={(hospitalname) => this.hospitalname = hospitalname} placeholder="Hospital Name"/>
          </div>
          <div className="form-group">
            <label>Hospital No</label>
            <input className="form-control" ref={(hospitalno) => this.hospitalno = hospitalno} placeholder="Hospital No"/>
          </div>
          <div className="form-group">
            <label>Hospital Address</label>
            <input className="form-control" ref={(hospitaladdress) => this.hospitaladdress = hospitaladdress} placeholder="Hospital Address"/>
          </div>
          <div className="form-group">
            <label>Hospital Pincode</label>
            <input className="form-control" ref={(pincode) => this.pincode = pincode} placeholder="Hospital Address"/>
          </div>
          <div>
          <button type="submit" className="btn btn-primary">Add Hospital</button>
          </div>
        </form>
      </div>
      </div>
    )
  }
}
