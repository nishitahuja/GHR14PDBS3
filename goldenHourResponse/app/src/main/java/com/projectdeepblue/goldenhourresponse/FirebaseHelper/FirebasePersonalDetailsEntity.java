package com.projectdeepblue.goldenhourresponse.FirebaseHelper;

import android.os.Bundle;

/**
 * Created by Omkar on 02-02-2018.
 */

public class FirebasePersonalDetailsEntity {
    String firstName;
    String lastName;
    String  contactno;
    Integer age;
    String gender;
    String bloodGroup;
    String disease;

    public FirebasePersonalDetailsEntity() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getContactno() {
        return contactno;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public String getDisease() {
        return disease;
    }

    public FirebasePersonalDetailsEntity(Bundle b) {
        this.firstName = b.getString("FIRST_NAME") ;
        this.lastName = b.getString("LAST_NAME") ;
        this.contactno =  b.getString("CONTACT") ;
        this.age =  Integer.parseInt(b.getString("AGE"));
        this.disease=b.getString("DISEASE");
        this.gender=b.getString("GENDER");
        this.bloodGroup=b.getString("BLOODGROUP");




    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }
}
