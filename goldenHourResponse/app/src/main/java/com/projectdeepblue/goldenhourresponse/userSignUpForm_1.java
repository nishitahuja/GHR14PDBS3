package com.projectdeepblue.goldenhourresponse;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class userSignUpForm_1 extends AppCompatActivity implements View.OnClickListener {

    private EditText userAge,userFirstName,userLastName,userContactno;
    private Spinner diseaseUser,bloodGroupUser,genderUser;
    private Button proceed ;
    Map<String,Object> userDataMap=new HashMap<>();
    private String age;
    private String address;
    private String name;
    Integer  contactno;
    private String selectedDisease;
    private String selectedGender;
    private String selectedBloodGroup;







    private MediaPlayer mMediaPlayer;
    private MediaSessionCompat mMediaSessionCompat;

    private BroadcastReceiver mNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if( mMediaPlayer != null && mMediaPlayer.isPlaying() ) {
                mMediaPlayer.pause();
            }
        }
    };
/*
    private MediaSessionCompat.Callback mMediaSessionCallback = new MediaSessionCompat.Callback() {


        @Override
        public boolean onMediaButtonEvent(Intent mediaButtonEvent){
            KeyEvent keyEvent = mediaButtonEvent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
            if (keyEvent == null || keyEvent.getAction() != KeyEvent.ACTION_DOWN)
                Toast.makeText(userSignUpForm_1.this, "Action down key", Toast.LENGTH_SHORT).show();
            return true;
        }
    };

    */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_sign_up_form_1);


        userAge=(EditText)findViewById(R.id.EditText_age);
        userFirstName=(EditText)findViewById(R.id.EditText_FirstName);
        userLastName=(EditText)findViewById(R.id.EditText_LastName);
        userContactno=(EditText)findViewById(R.id.EditText_Contact);

        proceed=(Button)findViewById(R.id.button_proceed);


        diseaseUser=(Spinner)findViewById(R.id.spinner_disease);
        bloodGroupUser=(Spinner)findViewById(R.id.spinner_Bloodgroup);
        genderUser=(Spinner)findViewById(R.id.spinner_Gender);

        diseaseUser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                 selectedDisease=(String) adapterView.getItemAtPosition(i);
                Toast.makeText(userSignUpForm_1.this, selectedDisease, Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bloodGroupUser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                 selectedBloodGroup=(String) adapterView.getItemAtPosition(i);
                Toast.makeText(userSignUpForm_1.this, selectedBloodGroup, Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        genderUser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                 selectedGender=(String) adapterView.getItemAtPosition(i);
                Toast.makeText(userSignUpForm_1.this, selectedGender, Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        proceed.setOnClickListener(this);



    }


    @Override
    public void onClick(View view) {
        if(view==proceed){
            registeruser1();
        }
    }

    private void registeruser1() {
        String firstName=userFirstName.getText().toString().trim();
        String contactno=userContactno.getText().toString().trim();
        String LastName=userLastName.getText().toString().trim();
        String age=userAge.getText().toString().trim();


        if(firstName.isEmpty()){
            userFirstName.setError("Must be filled");
            userFirstName.requestFocus();
            return;
        }
        if(LastName.isEmpty()){
            userLastName.setError("Must be filled");
            userLastName.requestFocus();
            return;
        }
        if(contactno.isEmpty()){
            userContactno.setError("Must be filled");
            userContactno.requestFocus();
            return;
        }
        if(age.isEmpty()){
            userAge.setError("Must be filled");
            userAge.requestFocus();
            return;
        }
        if(Integer.valueOf(age)>150|| Integer.valueOf(age) <=0)
        {
            userAge.setError("Invalid Age");
        }
        char[] cno=contactno.toCharArray();
        char[] temp=new char[10];
        int i=9;
        int j=cno.length-1;
        while(i>=0){
            temp[i]=cno[j];
            i--;
            j--;
        }
        contactno=String.valueOf(temp);



        finish();
        Bundle userPersonalDetials = new Bundle();
        Intent intent=new Intent(userSignUpForm_1.this,userSignUpForm_3_familyDetails.class);
        userPersonalDetials.putString("FIRST_NAME",firstName);
        userPersonalDetials.putString("CONTACT",contactno);
        userPersonalDetials.putString("LAST_NAME",LastName);
        userPersonalDetials.putString("AGE",age);


 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        userPersonalDetials.putString("GENDER",selectedGender);
        userPersonalDetials.putString("DISEASE",selectedDisease);
        userPersonalDetials.putString("BLOODGROUP",selectedBloodGroup);

        intent.putExtras(userPersonalDetials);
        startActivity(intent);
    }

/*

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch(keyCode){
            case KeyEvent.KEYCODE_POWER:
                Toast.makeText(this, "Power Button Overrided", Toast.LENGTH_SHORT).show();
                event.startTracking();
                return true;
            case KeyEvent.KEYCODE_MENU:
                Toast.makeText(this, "Menu key pressed", Toast.LENGTH_SHORT).show();
                return true;
            case KeyEvent.KEYCODE_SEARCH:
                Toast.makeText(this, "Search key pressed", Toast.LENGTH_SHORT).show();
                return false;
            case KeyEvent.KEYCODE_BACK:
                onBackPressed();
                return false;
            case KeyEvent.KEYCODE_VOLUME_UP:
                Toast.makeText(this, "Up key pressed", Toast.LENGTH_SHORT).show();
                event.startTracking();
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                Toast.makeText(this,"Volumen Down pressed", Toast.LENGTH_SHORT).show();
                return false;
        }
        return super.onKeyDown(keyCode, event);
    }






    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP) {
            //do nothing but show a warning message
            Toast.makeText(this, "you pressed the up button",Toast.LENGTH_SHORT).show();
            System.out.print("up button pressed");
            return true;
        }

        return super.dispatchKeyEvent(event);
    }

*/

}

