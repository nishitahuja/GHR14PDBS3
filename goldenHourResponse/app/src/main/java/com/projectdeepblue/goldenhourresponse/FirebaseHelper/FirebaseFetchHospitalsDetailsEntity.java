package com.projectdeepblue.goldenhourresponse.FirebaseHelper;

/**
 * Created by Omkar on 05-02-2018.
 */

public class FirebaseFetchHospitalsDetailsEntity {
    String hospitaladdress;
    String hospitalno;


    public FirebaseFetchHospitalsDetailsEntity(String hospitaladdress, String hospitalno) {
        this.hospitaladdress = hospitaladdress;
        this.hospitalno = hospitalno;
    }

    public FirebaseFetchHospitalsDetailsEntity() {
    }

    public String getHospitaladdress() {
        return hospitaladdress;
    }

    public String getHospitalno() {
        return hospitalno;
    }
}
