package com.projectdeepblue.goldenhourresponse;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.projectdeepblue.goldenhourresponse.FirebaseHelper.FirebaseFetchHospitalsDetailsEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class VictimHelpProviderTempActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String DEFAULT = "-NA-";       //needs to be changed
    private String[] relativeContact = new String[7];
    private String[] chilkey = new String[7];
    private String phoneNo;
    private Button iAmFine,BackButton;
    private FusedLocationProviderClient mFusedLocationClient;
    private String message;
    private long childrenCount;
    FirebaseAuth mAuthOnstart;
    private long messageContactLoggerCount;
    private String hospitalName;
    private long Loopcounter = 0;
    private String hospitalName1;
    private String hospitalName2;
    public String contactno1="-NA-";
    public String contactno2="-NA-";


    public ArrayList<String> Userlist;


    private String contactsMessage="";
    private String LocationValueForMessage;
    private String pinCodeValueForMessageAndDBQuery;
    protected Location mLastLocation;
    private String pincodeForSeacrhingDB;
    private String sendMyLocationText;
    private  double latitude;
    private double longitude;
    private String[] contactnoArray = new String[100];
    public String address;
    DatabaseReference myRef;
    DatabaseReference myRef2;
    boolean helpFlag;
    private int contactsListenerFlag=0;
    private int count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victim_help_provider_temp);




        // b = getIntent().getExtras();
        iAmFine = (Button) findViewById(R.id.Button_IamFine);
        BackButton=(Button)findViewById(R.id.Button_VictimHelpBack);
        //create a promise returned from a function to make things synchronous
        //  iAmFine.setOnClickListener(this);

        iAmFine.setOnClickListener(this);
        BackButton.setOnClickListener(this);

    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            // mLatitudeText.setText(String.valueOf(mLastLocation.getLatitude()));



                            latitude =mLastLocation.getLatitude();
                            longitude= mLastLocation.getLongitude();


                            String myLocality =getLocationInString(latitude,longitude);
                            // userLocationInformation userLocation=new userLocationInformation(latitude,longitude,myLocality);
                            // databaseReference.child("User Locations").child(user.getUid()).setValue(userLocation);


                            // mLatitudeText.setText(String.format(Locale.ENGLISH, "%s: %f",
                            ///   mLatitudeLabel,
                            // mLastLocation.getLatitude();
                            //mLongitudeText.setText(String.format(Locale.ENGLISH, "%s: %f",
                            //       mLongitudeLabel,
                            //j       mLastLocation.getLongitude()));

                        } else {
                            // Log.w("This activity", "getLastLocation:exception", task.getException());
                            Toast.makeText(VictimHelpProviderTempActivity.this, "LocationFailed to detect", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    public String getLocationInString(double latitude,double longitude) {
        String myLocality = null;

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

            if (addresses.isEmpty()) {
                // Log.i(TAG, "Waiting for Location");
            } else {
                if (addresses.size() > 0) {
                   /* Log.d(TAG, addresses.get(0).getFeatureName() + ", "
                            + addresses.get(0).getLocality() + ","
                            + addresses.get(0).getAdminArea() + ","
                            + addresses.get(0).getCountryName()+","+addresses.get(0).getPostalCode()+","+addresses.get(0).getPremises()+","+addresses.get(0).getSubAdminArea()+","+addresses.get(0).getSubLocality());
                   */

                    String locationDisplay =addresses.get(0).getAddressLine(0);
/*
                            addresses.get(0).getFeatureName() + ", "
                            + addresses.get(0).getLocality() + ","
                            + addresses.get(0).getAdminArea() + ","
                            + addresses.get(0).getCountryName()+","+addresses.get(0).getPostalCode()+","+addresses.get(0).getPremises()+","+addresses.get(0).getSubAdminArea()+
                            ","+addresses.get(0).getSubLocality();
                            */
                    sendMyLocationText=
                            addresses.get(0).getThoroughfare()+","+addresses.get(0).getSubLocality()+","+

                                    addresses.get(0).getPostalCode();
                    ;
                    pincodeForSeacrhingDB=addresses.get(0).getPostalCode();
                    // Log.i("Locationwa",sendMyLocationText+pincodeForSeacrhingDB);
                    new A().execute();


                    // pinCodeValueForMessageAndDBQuery=pincodeForSeacrhingDB;
                    //  LocationValueForMessage=sendMyLocationText;



                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return  myLocality;
    }

    @Override
    public void onClick(View view) {
        if (view == iAmFine) {
            for (int i = 0; i <= 1; i++) {
                phoneNo = relativeContact[i];
                message = "I AM FINE ,PLEASE DO NOT PANIC.THE MESSAGE WAS SENT BY MISTAKE";
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, message, null, null);
                Toast.makeText(getApplicationContext(), "RELATIVES INFORMED ABOUT WRONG MESSAGE.",
                        Toast.LENGTH_LONG).show();
            }
        }
        if(view==BackButton)
        {
            Intent homeIntent = new Intent(VictimHelpProviderTempActivity.this,homeScreenAfterLogin.class);
            startActivity(homeIntent);
        }
    }


    class A extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground(Void... params) {
            callHospitals();


            SharedPreferences sharedPreferences=getSharedPreferences("RelativeData", Context.MODE_PRIVATE);
            count=( sharedPreferences.getAll().size())/2;

            for (int i = 0; i <( sharedPreferences.getAll().size())/2; i++) {
                relativeContact[i] = sharedPreferences.getString("relativeNumber" + i, DEFAULT);
            }
            Log.i("contact", relativeContact[0]);



            //sendSMSMessage();
            return  null;
        }

        protected void onProgressUpdate(Void... progress) {

        }

        protected void onPreExecute()
        {

        }

        protected void onPostExecute(Void result) {

            Log.i("contacts_guys",contactno1+contactno2);
            //new B().execute();
            Log.i("onPostExecute","Done execution kids");
        }

    }



    private void callHospitals()
    {

        final DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        myRef = FirebaseDatabase.getInstance().getReference().child("hospital").child(pincodeForSeacrhingDB.toString());
        myRef2=FirebaseDatabase.getInstance().getReference().child("hospital");

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        helpFlag=true;
        mDatabaseReference.child("USERS").child(user.getUid()).child("helpFlag").setValue(helpFlag);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        myRef2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        myRef2.addValueEventListener(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                          checking if hospital db is present or not
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if(dataSnapshot.hasChild(pincodeForSeacrhingDB.toString())) {
                    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Userlist = new ArrayList<String>();
                            // Result will be holded Here
                            for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                                Userlist.add(String.valueOf(dsp.getKey())); //add result into array list

                            }
                            Log.i("getValue",Userlist.get(1).toString());

                            hospitalName1=Userlist.get(0);
                            hospitalName2=Userlist.get(1);



                            myRef.child(Userlist.get(0).toString()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    FirebaseFetchHospitalsDetailsEntity data = dataSnapshot.getValue(FirebaseFetchHospitalsDetailsEntity.class);
                                    contactno1 = data.getHospitalno();
                                    Log.i("contactNo1",contactno1);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            myRef.child(Userlist.get(1).toString()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    FirebaseFetchHospitalsDetailsEntity data = dataSnapshot.getValue(FirebaseFetchHospitalsDetailsEntity.class);
                                    contactno2 = data.getHospitalno();
                                    Log.i("contactNo2",contactno2);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                            Log.i("contacts_guys",contactno1+contactno2);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                }
                else
                {
                    Log.i("FAILchild","CHILD DOES NOT EXIST");

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }



    private void collectPhoneNumbers(Map<String,Object> users) {

        ArrayList<Long> phoneNumbers = new ArrayList<>();

        //iterate through each user, ignoring their UID
        for (Map.Entry<String, Object> entry : users.entrySet()){

            //Get user map
            Map singleUser = (Map) entry.getValue();
            //Get phone field and append to list

            //phoneNumbers.add((Long) singleUser.get("phone"));
        }

        System.out.println(phoneNumbers.toString());
        Log.i("phoneNumberstoString()",phoneNumbers.toString());
    }

    protected void sendSMSMessage() {


        // Log.i("name2",contactno );


        for (int i = 0; i < count; i++) {
            phoneNo = relativeContact[i];
            message = "I NEED HELP ,My Location:"+ sendMyLocationText +"."+"Nearby Hospitals : "+contactsMessage;
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Log.i("messsageForRelatives", message);
            //Toast.makeText(getApplicationContext(), "SMS sent.",
            // Toast.LENGTH_LONG).show();

        }
    }



    protected void onStart() {

        super.onStart();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(VictimHelpProviderTempActivity.this);
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getBaseContext());
        if (status != ConnectionResult.SUCCESS) {
            int requestCode = 10;
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(VictimHelpProviderTempActivity.this, status, requestCode);
            dialog.show();
        } else {

            // Need to fill up with reference and new intent
            getLastLocation();

        }
    }

}
