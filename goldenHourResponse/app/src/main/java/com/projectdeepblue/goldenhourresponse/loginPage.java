package com.projectdeepblue.goldenhourresponse;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.auth.ResultCodes;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.projectdeepblue.goldenhourresponse.CheckingConnectivity.ConnectionStatus;
import com.projectdeepblue.goldenhourresponse.CheckingConnectivity.ConnectionStatusGPS;

import java.util.Arrays;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.SEND_SMS;

public class loginPage extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    public static final int RequestPermissionCode = 7;
    private Button login;
    private EditText emailInput;
    private Button googleSignIn;
    private ProgressDialog mProgressDialog;

    private EditText passwordInput;
    private String idToken;
    private String name, email;
    private Button createNewAccount;


    private String  username;
    private FirebaseAuth firebaseAuth;

    private FirebaseAuth authSignIn;
    private FirebaseAuth.AuthStateListener mAuthListener;


    private FirebaseFirestore mFirestore;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;




    private View mRootView;
    GoogleApiClient mGoogleApiCLient;


    private static final int RC_SIGN_IN=9001;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);



        authSignIn= FirebaseAuth.getInstance();
        mRootView = findViewById(R.id.loginPagexml);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //              web client change left

 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Configure sign-in to request the user's ID, email address, and basic
// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(loginPage.this.getResources().getString(R.string.release_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiCLient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        //mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        login = (Button)findViewById(R.id.loginButton);
        emailInput=(EditText)findViewById(R.id.emailEditText);
        passwordInput= (EditText)findViewById(R.id.passwordEditText);
        googleSignIn=(Button)findViewById(R.id.Button_googleSignIn) ;
        createNewAccount=(Button)findViewById(R.id.Button_create_new_account) ;

       // mAuthListener = new FirebaseAuth.AuthStateListener() {
           // @Override
           // public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //Get signedIn user
               // FirebaseUser user=firebaseAuth.getCurrentUser();

               // if(user!=null){
                    //user is signed using the proper credentials
                //    createUserInFirebaseHelper();
                    //Log.d("Google Sign in with id",user.getUid());

             //   }
              //  else
              //  {
                  //  Log.d("logged out","onAuthStateChanged:signed_out");
              //  }

          //  }

      //  };
        if(authSignIn.getCurrentUser() != null) {
            finish();
            Intent i = new Intent(loginPage.this, homeScreenAfterLogin.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
        login.setOnClickListener(this);
        googleSignIn.setOnClickListener(this);
        createNewAccount.setOnClickListener(this);

    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                              // Handling Button Events

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onClick(View view) {
        if(view==login)
        {
            userSignedIn();
        }
        if(view==googleSignIn){
            signIn();
        }
        if(view ==createNewAccount)
        {
            Intent intent = new Intent(loginPage.this, newUserSignUp.class);
            startActivity(intent);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Check for existing Google Sign In account, if the user is already signed in
// the GoogleSignInAccount will be non-null.
       GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
       ConnectionStatus internetConnectionStatus = new ConnectionStatus(getApplicationContext());
        ConnectionStatusGPS GPSconnectionStatus=new ConnectionStatusGPS(getApplicationContext());

        // Check if Internet present
        if (!internetConnectionStatus.isConnectingToInternet()) {
            // Internet Connection is not present
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Connect to wifi or quit")
                    .setCancelable(false)
                    .setPositiveButton("Connect to WIFI", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        }
                    })
                    .setNegativeButton("Stay Offline", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Toast.makeText(loginPage.this, "Please Turn On WIFI", Toast.LENGTH_SHORT).show();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

            return;
        }

        try {
            if (!GPSconnectionStatus.isGPSOn()) {
                // Internet Connection is not present
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Turn on Location Services")
                        .setCancelable(false)
                        .setPositiveButton("Open Settings", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(onGPS);
                            }
                        })
                        .setNegativeButton("Stay Offline", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(loginPage.this, "Please Turn On WIFI", Toast.LENGTH_SHORT).show();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

                return;
            }
        } catch (Settings.SettingNotFoundException e) {
            Toast.makeText(loginPage.this, "Cannot Open Settings", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        // If All permission is enabled successfully then this block will execute.
        if(CheckingPermissionIsEnabledOrNot())
        {
            Toast.makeText(loginPage.this, "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
        }

        // If, If permission is not enabled then else condition will execute.
        else {

            //Calling method to enable permission.
            RequestMultiplePermission();

        }

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
           GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        }
     //   if (requestCode == 777) {
     //       handleSignInResponse(resultCode, data);
     //   }
    }




    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {

            GoogleSignInAccount account = result.getSignInAccount();

            idToken = account.getIdToken();

            name = account.getDisplayName();
            email = account.getEmail();
            AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
            firebaseAuthWithGoogle(credential);

        }

            else
            {
                // Google Sign In failed, update UI appropriately
                Log.e("failed", "Login Unsuccessful. ");
                Toast.makeText(this, "Login Unsuccessful", Toast.LENGTH_SHORT)
                        .show();

            }
        }

    //After a successful sign into Google, this method now authenticates the user with Firebase
    private void firebaseAuthWithGoogle(AuthCredential credential){
        showProgressDialog();
        authSignIn.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("tag", "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                          //  Log.w("tag", "signInWithCredential" + task.getException().getMessage());
                            task.getException().printStackTrace();
                            Toast.makeText(loginPage.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }else {

                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference mDatabaseReference = database.getReference();

                            FirebaseUser user= authSignIn.getInstance().getCurrentUser();
                            try {
                                //  DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
                                mDatabaseReference.child("USERS").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists())
                                        {
                                            Intent secondScreenIntetnt= new Intent(loginPage.this,homeScreenAfterLogin.class);
                                            startActivity(secondScreenIntetnt);
                                        }
                                        else
                                        {
                                            Intent secondScreenIntetnt= new Intent(loginPage.this,userSignUpForm_1.class);
                                            startActivity(secondScreenIntetnt);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                Toast.makeText(loginPage.this, "Exception Generated", Toast.LENGTH_SHORT).show();
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                // put some code to execute instead of forcing the application to force shutdown
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            }
                            hideProgressDialog();

                        }
                    }
                });
    }


    private void signIn() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiCLient);
        startActivityForResult(signInIntent,RC_SIGN_IN);
    }

    private void userSignedIn(){

        final String userEmail=emailInput.getText().toString().trim();
        String userPassword=passwordInput.getText().toString().trim();
        if(TextUtils.isEmpty(userEmail))
        {
            Toast.makeText(this, "Enter Email Address", Toast.LENGTH_SHORT).show();
            return;

        }
        if(TextUtils.isEmpty(userPassword))
        {
            Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show();
            return;
        }
        showProgressDialog();

      //  progressDialog.setMessage("Please Wait");

      // progressDialog.show();


        authSignIn.signInWithEmailAndPassword(userEmail,userPassword)
                .addOnCompleteListener(loginPage.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference mDatabaseReference = database.getReference();

                            FirebaseUser user= authSignIn.getInstance().getCurrentUser();
                            try {
                                //  DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
                                mDatabaseReference.child("USERS").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists())
                                        {
                                            Intent secondScreenIntetnt= new Intent(loginPage.this,homeScreenAfterLogin.class);
                                            startActivity(secondScreenIntetnt);
                                        }
                                        else
                                        {
                                            Intent secondScreenIntetnt= new Intent(loginPage.this,userSignUpForm_1.class);
                                            startActivity(secondScreenIntetnt);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                Toast.makeText(loginPage.this, "Exception Generated", Toast.LENGTH_SHORT).show();
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                // put some code to execute instead of forcing the application to force shutdown
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            }

                        }
                        else
                        {
                            Toast.makeText(loginPage.this, "Login Failed", Toast.LENGTH_SHORT).show();
                            hideProgressDialog();

                        }
                    }
                });

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }



    private void handleSignInResponse(int resultCode, Intent data) {
        IdpResponse response = IdpResponse.fromResultIntent(data);

        if (resultCode == ResultCodes.OK) {
            goMainScreen();
        } else {
            if (response == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
                return;
            }
            if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
              //  showSnackbar(R.string.no_internet_connection);
                Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
                return;
            }
            if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
               // showSnackbar(R.string.unknown_error);
                Toast.makeText(this, "unknown_error", Toast.LENGTH_SHORT).show();

                return;
            }
        }
      //  showSnackbar(R.string.unknown_sign_in_response);
        Toast.makeText(this, "unknown_sign_in_response", Toast.LENGTH_SHORT).show();
    }

    public void signInPhone(View view) {
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(
                                Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.PHONE_VERIFICATION_PROVIDER).build()))
                        .build(),
                777);
    }

    private void goMainScreen() {
        Intent intent = new Intent(this, homeScreenAfterLogin.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void showSnackbar(int errorMessageRes) {
        Snackbar.make(mRootView, errorMessageRes, Snackbar.LENGTH_LONG).show();
    }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                                     // Location Permissions

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            //  Log.i("this activity", "Displaying permission rationale to provide additional context.");


            // Request permission
            startLocationPermissionRequest();


        } else {
            // Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(loginPage.this,
                new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);



    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                                     // Sending Messages Permission

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private boolean checkMessagesPermission() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


    private void requestMessagePermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.SEND_SMS);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            //  Log.i("this activity", "Displaying permission rationale to provide additional context.");


            // Request permission
            startMessagePermissionRequest();


        } else {
            // Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startMessagePermissionRequest();
        }


    }

    private void startMessagePermissionRequest() {
        ActivityCompat.requestPermissions(loginPage.this,
                new String[]{android.Manifest.permission.SEND_SMS},
                REQUEST_PERMISSIONS_REQUEST_CODE);


    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // ALL Permissions

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Permission function starts from here
    private void RequestMultiplePermission() {

        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(loginPage.this, new String[]
                {
                        ACCESS_COARSE_LOCATION,
                        ACCESS_FINE_LOCATION,
                        SEND_SMS,
                        READ_CONTACTS,CALL_PHONE
                }, RequestPermissionCode);

    }

    // Calling override method.
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCode:

                if (grantResults.length > 0) {

                    boolean LocationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean Location2Permission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean SendSMSPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadContactPermission = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean CallPhonPermissions=  grantResults[4] == PackageManager.PERMISSION_GRANTED;

                    if (LocationPermission && Location2Permission && SendSMSPermission && ReadContactPermission && CallPhonPermissions) {

                        Toast.makeText(loginPage.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(loginPage.this,"Please Enable permissions for Emergency Situations",Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    // Checking permission is enabled or not using function starts from here.
    public boolean CheckingPermissionIsEnabledOrNot() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), SEND_SMS);
        int ForthPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_CONTACTS);
        int FifthPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE);

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ForthPermissionResult == PackageManager.PERMISSION_GRANTED &&
                FifthPermissionResult == PackageManager.PERMISSION_GRANTED ;
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                                             // progress bar display

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Signing In");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
