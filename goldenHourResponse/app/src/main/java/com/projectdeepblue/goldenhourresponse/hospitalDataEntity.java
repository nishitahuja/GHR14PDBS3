package com.projectdeepblue.goldenhourresponse;

/**
 * Created by Omkar on 16-01-2018.
 */

public class hospitalDataEntity {
    public String name;
    public String contactno;
    public String address;

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public String getContactno() {
        return contactno;
    }
}

