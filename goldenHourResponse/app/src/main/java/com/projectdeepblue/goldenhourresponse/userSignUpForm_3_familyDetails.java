package com.projectdeepblue.goldenhourresponse;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.projectdeepblue.goldenhourresponse.FirebaseHelper.FirebasePersonalDetailsEntity;
import com.projectdeepblue.goldenhourresponse.FirebaseHelper.FirebaseRelativeDetailsEntity;
import com.projectdeepblue.goldenhourresponse.RecyclerView.ContactsDisplayAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.SEND_SMS;

// Import permission  files.

public class userSignUpForm_3_familyDetails extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_READ_CONTACTS = 444;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    public static final int RequestPermissionCode = 7;





    boolean contactsExceeded;
    private Button saveAndProceed,selectContacts;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    int ad;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////






    Bundle userDetials;

    private RecyclerView recyclerView;
    private List<ContactsListItem> contactsList;
    private  RecyclerView.Adapter adapter;
    private int i=0;
    Map<Object,String> mMap=new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_sign_up_form_3_family_details);


        recyclerView=(RecyclerView)findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        contactsList=new ArrayList<>();
        adapter=  new ContactsDisplayAdapter(contactsList,this);

        userDetials = getIntent().getExtras();


        //  firstRelativeName=(EditText)findViewById(R.id.editText_FirstRelativeFullName);
        // secondRelativeName=(EditText)findViewById(R.id.editText_secondRelativeFullName);
        // firstRelativeContactno=(EditText)findViewById(R.id.R1Contact);
        //secondRelativeContact=(EditText)findViewById(R.id.R2contact);

        saveAndProceed = (Button)findViewById(R.id.signup_complete);
        selectContacts = (Button)findViewById(R.id.select_contacts);


        saveAndProceed.setOnClickListener(this);
        selectContacts.setOnClickListener(this);




    }

    @Override
    public void onClick(View view) {
        if(view==saveAndProceed)
        {
            if(contactsExceeded==true)
            {
                contactsExceeded=false;
                Toast.makeText(this, "Please select only 5 contacts", Toast.LENGTH_SHORT).show();
            }
            else{
                registerUser2();

            }

        }
        if(view==selectContacts){
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, 1);
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //                              need to add request permission

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onActivityResult( ,Activity.RESULT_OK,);
            }
        }
    }
    */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            Uri contactData = data.getData();
            Cursor c =  getContentResolver().query(contactData, null, null, null, null);
            if (c.moveToFirst()) {

                String phoneNumber="",emailAddress="";
                String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                if ( hasPhone.equalsIgnoreCase("1"))
                    hasPhone = "true";
                else
                    hasPhone = "false" ;

                if (Boolean.parseBoolean(hasPhone))
                {
                    Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ contactId,null, null);
                    while (phones.moveToNext())
                    {
                        phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }
                    phones.close();
                }

                // Find Email Addresses
                Cursor emails = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,null,ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId,null, null);
                while (emails.moveToNext())
                {
                    emailAddress = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                }
                emails.close();

                //mainActivity.onBackPressed();
                // Toast.makeText(mainactivity, "go go go", Toast.LENGTH_SHORT).show();



                ContactsListItem listItem= new ContactsListItem(
                        name,phoneNumber);

                contactsList.add(listItem);
              //  userDetials.putString("RELATIVE_NAME"+i,name);

              //  userDetials.putString("RELATIVE_CONTACT"+i,phoneNumber);
                i++;
            }
            recyclerView.setAdapter(adapter);

            if(adapter.getItemCount()>5)
            {
                Toast.makeText(this, "5 Relatives Selected Already", Toast.LENGTH_SHORT).show();
                contactsExceeded=true;
            }
            else {
                contactsExceeded=false;
            }
            c.close();
        }
    }


    private void registerUser2() {

        SharedPreferences sharedPreferences=getSharedPreferences("RelativeData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        sharedPreferences.edit().clear().commit();


           for(int i=0;i <adapter.getItemCount();i++){
             //  Log.i("RELATIVE_NAME"+i,contactsList.get(i).getHead());
             //  Log.i("RELATIVE_CONTACT"+i,contactsList.get(i).getDesc());
                editor.putString("relativeName"+i,contactsList.get(i).getHead());
                editor.putString("relativeNumber"+i,contactsList.get(i).getDesc());
                editor.commit();

               userDetials.putString("RELATIVE_NAME"+i,contactsList.get(i).getHead());
               userDetials.putString("RELATIVE_CONTACT"+i,contactsList.get(i).getDesc());

           }

        Intent intent=new Intent(userSignUpForm_3_familyDetails.this,homeScreenAfterLogin.class);

        //  userDetials.putString("FIRST_RELATIVE_NAME","Omkar Tulaskar");
        //   userDetials.putString("SECOND_RELATIVE_NAME","helo2");
        //   userDetials.putString("FIRST_RELATIVE_CONTACT","44");
        //  userDetials.putString("SECOND_RELATIVE_CONTACT","95544");

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mDatabaseReference = database.getReference();

        FirebaseAuth FirebaseAuthentication= FirebaseAuth.getInstance();
        FirebaseUser user =FirebaseAuthentication.getCurrentUser();

        FirebasePersonalDetailsEntity personalDetailsEntityObject = new FirebasePersonalDetailsEntity(userDetials);
        FirebaseRelativeDetailsEntity relativeDetailsEntityObject= new FirebaseRelativeDetailsEntity(userDetials);

            try {
                //   mDatabaseReference = FirebaseDatabase.getInstance().getReference();
                mDatabaseReference.child("USERS").child(user.getUid()).child("name").setValue(userDetials.getString("FIRST_NAME")+" "+userDetials.getString("LAST_NAME"));
                mDatabaseReference.child("USERS").child(user.getUid()).child("PERSONAL_DETAILS").setValue(personalDetailsEntityObject);
                for(int i=0;i<=adapter.getItemCount();i++) {
                    mDatabaseReference.child("RELATIVE_DB").child(user.getUid()).child("relativeName" + i).setValue(userDetials.getString("RELATIVE_NAME" + i));
                    char[] cno=userDetials.getString("RELATIVE_CONTACT" + i).toCharArray();
                    char[] temp=new char[10];
                    int k=9;
                    int j=cno.length-1;
                    while(k>=0){
                        temp[k]=cno[j];
                        k--;
                        j--;
                    }
                    String contactno=String.valueOf(temp);
                    mDatabaseReference.child("RELATIVE_DB").child(user.getUid()).child("relativeNumber" + i).setValue(contactno);
                    //mDatabaseReference.child("RELATIVE_DB").child(user.getUid()).setValue(relativeDetailsEntityObject);
                }
                    Log.i("tag","updated bro");
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(this, "Sorry There was problem updating details." +
                        "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // put some code to execute instead of forcing the application to force shutdown
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(intent);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
    @Override
    protected void onStart() {

        super.onStart();


        // If All permission is enabled successfully then this block will execute.
        if(CheckingPermissionIsEnabledOrNot())
        {
            Toast.makeText(userSignUpForm_3_familyDetails.this, "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
        }

        // If, If permission is not enabled then else condition will execute.
        else {

            //Calling method to enable permission.
            RequestMultiplePermission();

        }


    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                             // ALL Permissions

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Permission function starts from here
    private void RequestMultiplePermission() {

        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(userSignUpForm_3_familyDetails.this, new String[]
                {
                        ACCESS_COARSE_LOCATION,
                        ACCESS_FINE_LOCATION,
                        SEND_SMS,
                        READ_CONTACTS,CALL_PHONE
                }, RequestPermissionCode);

    }

    // Calling override method.
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCode:

                if (grantResults.length > 0) {

                    boolean LocationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean Location2Permission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean SendSMSPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadContactPermission = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean CallPhonPermissions=  grantResults[4] == PackageManager.PERMISSION_GRANTED;

                    if (LocationPermission && Location2Permission && SendSMSPermission && ReadContactPermission && CallPhonPermissions) {

                        Toast.makeText(userSignUpForm_3_familyDetails.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(userSignUpForm_3_familyDetails.this,"Please Enable permissions for Emergency Situations",Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    // Checking permission is enabled or not using function starts from here.
    public boolean CheckingPermissionIsEnabledOrNot() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), SEND_SMS);
        int ForthPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_CONTACTS);
        int FifthPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE);

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ForthPermissionResult == PackageManager.PERMISSION_GRANTED &&
                FifthPermissionResult == PackageManager.PERMISSION_GRANTED ;
    }

}
