package com.projectdeepblue.goldenhourresponse.Firestore;

import android.os.Bundle;

/**
 * Created by Omkar on 19-01-2018.
 */

public class userRelativeEntity {
    public String firstRelativeName;
    public String  secondRelativeName;
    public String firstRelativeContact;
    public   String  secondRelativeContact;
    public String thirdRelativeName;
    public String  fourthRelativeName;
    public String thirdRelativeContact;
    public   String  fourthRelativeContact;
    public String   fifthRelativeName;
    public   String  sixthRelativeName;
    public String fifthRelativeContact;
    public   String  sixthRelativeContact;

    public userRelativeEntity() {
    }

    public userRelativeEntity(Bundle b) {
        this.firstRelativeName = b.getString("RELATIVE_NAME0");
        this.secondRelativeName = b.getString("RELATIVE_NAME1");
        this.firstRelativeContact = b.getString("RELATIVE_CONTACT0");
        this.secondRelativeContact = b.getString("RELATIVE_CONTACT1");
        this.thirdRelativeName = b.getString("RELATIVE_NAME3");
        this.fourthRelativeName = b.getString("RELATIVE_NAME4");
        this.thirdRelativeContact = b.getString("RELATIVE_CONTACT3");
        this.fourthRelativeContact = b.getString("RELATIVE_CONTACT4");
        this.fifthRelativeName = b.getString("RELATIVE_NAME5");
        this.sixthRelativeName = b.getString("RELATIVE_NAME6");
        this.fifthRelativeContact = b.getString("RELATIVE_CONTACT5");
        this.sixthRelativeContact = b.getString("RELATIVE_CONTACT6");
    }



/*
    public userRelativeEntity(Bundle b) {
        this.firstRelativeName = b.getString("RELATIVE_NAME0") ;
        this.firstRelativeContact = b.getString("RELATIVE_CONTACT0") ;
        this.secondRelativeName=b.getString("SECOND_RELATIVE_NAME");
       this.secondRelativeContact =  b.getString("SECOND_RELATIVE_CONTACT");
    }
    */
}