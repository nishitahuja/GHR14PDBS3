package com.projectdeepblue.goldenhourresponse.Firestore;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

/**
 * Created by Omkar on 03-01-2018.
 */

public class FirestoreAddUserDataHelper {

    public FirestoreAddUserDataHelper(){

    }

    private FirebaseFirestore mFirestore;
    private FirebaseAuth mFirebaseAuth;
    private String id;

    public FirestoreAddUserDataHelper(FirebaseFirestore mFirestore, FirebaseAuth mFirebaseAuth, String id) {
        this.mFirestore = mFirestore;
        this.mFirebaseAuth = mFirebaseAuth;
        this.id = id;
    }

    public void updateData(String name, String address, String contactno,Integer age)
    {
        UserEntity userDetails= new UserEntity(name,address,contactno,age,"true");
        FirebaseUser user= mFirebaseAuth.getInstance().getCurrentUser();
        id=user.getUid();

        mFirestore = FirebaseFirestore.getInstance();
        mFirestore.collection("users").document(id).set(userDetails).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
              e.printStackTrace();
            }
        });

    }
    public void updateDetails(Map map)
    {
        FirebaseUser user= mFirebaseAuth.getInstance().getCurrentUser();
        id=user.getUid();

        UserEntity userDetails= new UserEntity("true",map.get("userName").toString(),map.get("userContactno").toString(),map.get("completeUserAddress").toString(),Integer.parseInt(map.get("userAge").toString()));
        mFirestore = FirebaseFirestore.getInstance();
        mFirestore.collection("users").document(id).set(userDetails).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();

            }
        });
       // Log.i("update data","Success");
       // Toast.makeText((Context)com.omkarexample.emergencyhelp.userSignUpForm_2, "Update Successful", Toast.LENGTH_SHORT).show();
    }

    public void updateRelativeDetails(Bundle b){
        FirebaseUser user= mFirebaseAuth.getInstance().getCurrentUser();
        id=user.getUid();
        mFirestore = FirebaseFirestore.getInstance();
      userRelativeEntity UserRelativeEntity = new userRelativeEntity(b);
        mFirestore.collection("users").document(id).collection("userInformation").document("RelativeDetails").set(UserRelativeEntity).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();

            }
        });
       // Log.i("update data","Success");
        // Toast.makeText((Context)com.omkarexample.emergencyhelp.userSignUpForm_2, "Update Successful", Toast.LENGTH_SHORT).show();
    }



    public void updatePersonalDetails(Bundle b)
    {
        FirebaseUser user= mFirebaseAuth.getInstance().getCurrentUser();
        id=user.getUid();
        mFirestore = FirebaseFirestore.getInstance();
        personalDetailsEntity userEntity2=new personalDetailsEntity(b);
        mFirestore.collection("users").document(id).collection("userInformation").document("personalDetails").set(userEntity2).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();

            }
        });
     //   Log.i("update data","Success");
        // Toast.makeText((Context)com.omkarexample.emergencyhelp.userSignUpForm_2, "Update Successful", Toast.LENGTH_SHORT).show();
    }
    }


