package com.projectdeepblue.goldenhourresponse.Firestore;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * Created by Omkar on 08-01-2018.
 */

public class FirestoreRetriveUserDataHelper {
    private FirebaseFirestore mFirestore;
    private FirebaseAuth mFirebaseAuth;
    private String id;

    private int flag;
    public String username;


    public FirestoreRetriveUserDataHelper(FirebaseFirestore mFirestore, FirebaseAuth mFirebaseAuth, String id, int flag, String username) {
        this.mFirestore = mFirestore;
        this.mFirebaseAuth = mFirebaseAuth;
        this.id = id;
        this.flag = flag;
        this.username = username;
    }

    public FirestoreRetriveUserDataHelper(){

    }


    public String getData()
    {

        FirebaseUser user= mFirebaseAuth.getInstance().getCurrentUser();
        id=user.getUid();
        mFirestore = FirebaseFirestore.getInstance();
        mFirestore.collection("users").document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot documentSnapshot=task.getResult();
                     username = documentSnapshot.getString("name");
                   // Log.i("name",username);
                    flag=1;

                }else{
                    username="Unknown";
                    int flag= 0;
                }
            }
        });
        return username;
    }
}
