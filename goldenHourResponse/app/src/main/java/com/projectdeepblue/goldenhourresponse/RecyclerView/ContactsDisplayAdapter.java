package com.projectdeepblue.goldenhourresponse.RecyclerView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.projectdeepblue.goldenhourresponse.ContactsListItem;
import com.projectdeepblue.goldenhourresponse.R;

import java.util.List;



public class ContactsDisplayAdapter extends RecyclerView.Adapter<ContactsDisplayAdapter.ViewHolder> {

    private List<ContactsListItem> listItems;
    private Context context;

    public ContactsDisplayAdapter(List<ContactsListItem> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @Override
    public ContactsDisplayAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_contacts,parent,false);
        return  new ContactsDisplayAdapter.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ContactsDisplayAdapter.ViewHolder holder, final int position) {

        ContactsListItem listItem= listItems.get(position);

        holder.head.setText(listItem.getHead().toString());
        holder.desc.setText(listItem.getDesc().toString());
        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listItems.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, listItems.size());
                //holder.itemView.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView head;
        public TextView desc;
        public Button cancel;

        public ViewHolder(View itemView) {
            super(itemView);

            head=(TextView)itemView.findViewById(R.id.TextViewContactsName);
            desc=(TextView)itemView.findViewById(R.id.TextViewContactsNumber);
            cancel=(Button)itemView.findViewById(R.id.ButtonCancelContact);

        }
    }

}
