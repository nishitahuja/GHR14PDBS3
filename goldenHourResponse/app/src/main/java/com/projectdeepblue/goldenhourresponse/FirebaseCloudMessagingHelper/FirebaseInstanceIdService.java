package com.projectdeepblue.goldenhourresponse.FirebaseCloudMessagingHelper;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by Omkar on 08-02-2018.
 */

public class FirebaseInstanceIdService  extends com.google.firebase.iid.FirebaseInstanceIdService{
    private static final String REG_TOKEN = "REG_TOKEN";

    @Override
    public void onTokenRefresh()
    {
        String recent_token= FirebaseInstanceId.getInstance().getToken();
        Log.i("serviceTEST",recent_token);

    }
}
