package com.projectdeepblue.goldenhourresponse;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.projectdeepblue.goldenhourresponse.CheckingConnectivity.ConnectionStatus;
import com.projectdeepblue.goldenhourresponse.CheckingConnectivity.ConnectionStatusGPS;
import com.projectdeepblue.goldenhourresponse.FirebaseHelper.FirebasePersonalDetailsEntity;

import java.util.List;
import java.util.Locale;

public class homeScreenAfterLogin extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private Button testGetHospitals;
    private static final String TAG = homeScreenAfterLogin.class.getSimpleName();

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    /**
     * Provides the entry point to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Represents a geographical location.
     */

    private FirebaseFirestore db;
    private FirebaseAuth mAuthOnstart;
    private FirebaseAuth mAuth;
    private String id;

    private String phoneNo;
    private String message;
    private String[] relativeName = new String[7];
    private String[] relativeContact = new String[7];
    private String sendMyLocationText;
    public String getPhoneNumberForFCM = "hello";


    private int flag;
    public String username;

    private String hospitalName;
    String[] placeReferences = new String[70];
    protected Location mLastLocation;

    private double latitude;
    private double longitude;
    private int numberOfNearbyPlaces;
    private String pincodeForSeacrhingDB;


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //                                  Check for Internet Connecttion and do something is not connected


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 0;


    private ImageView victimButton1;
    private ImageView victimButton2;
    private ImageView helperButton;

    private TextView locationDisplayText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen_after_login);
        mAuth = FirebaseAuth.getInstance();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        victimButton1 = (ImageView) findViewById(R.id.Button_victimEmergencyHelpIcon1);
        victimButton2 = (ImageView) findViewById(R.id.Button_victimEmergencyHelpIcon2);
        helperButton = (ImageView) findViewById(R.id.helperIcon);
        locationDisplayText = (TextView) findViewById(R.id.locationDisplayTextView);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////















   /*

   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //          This block of code creating problem when logged in with new user as the asynchronouse behaviour of the
        //          firebase doesnt load up the relatives information and throws exception

   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("RELATIVE_DB").child(user.getUid());

        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FirebaseRelativeDetailsEntity data = dataSnapshot.getValue(FirebaseRelativeDetailsEntity.class);

                relativeContact[0] = data.getRelativeNumber1().toString();

                relativeContact[1] = data.getRelativeNumber2().toString();
                Log.i("numbers",relativeContact[0]);
                Log.i("numbers",relativeContact[1]);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        */

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getBaseContext());
        if (status != ConnectionResult.SUCCESS) {
            int requestCode = 10;
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(this, status, requestCode);
            dialog.show();
        } else {

            // Need to fill up with reference and new intent
            getLastLocation();

        }
        victimButton1.setOnClickListener(this);
        helperButton.setOnClickListener(this);
        victimButton2.setOnClickListener(this);
    }


    @Override
    public void onStart() {
        super.onStart();

        Animation mAnimation = new AlphaAnimation(1, 0);
        mAnimation.setDuration(200);
        mAnimation.setInterpolator(new LinearInterpolator());
        mAnimation.setRepeatCount(Animation.INFINITE);
        mAnimation.setRepeatMode(Animation.REVERSE);
        victimButton2.startAnimation(mAnimation);

        mAuthOnstart = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuthOnstart.getCurrentUser();
        if (currentUser == null) {
            Intent loginIntent = new Intent(homeScreenAfterLogin.this, loginPage.class);
            startActivity(loginIntent);
        }


        ConnectionStatus internetConnectionStatus = new ConnectionStatus(getApplicationContext());
        ConnectionStatusGPS GPSconnectionStatus = new ConnectionStatusGPS(getApplicationContext());

        // Check if Internet present
        if (!internetConnectionStatus.isConnectingToInternet()) {
            // Internet Connection is not present
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Connect to Internet or quit")
                    .setCancelable(false)
                    .setPositiveButton("Connect to Internet", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        }
                    })
                    .setNegativeButton("Stay Offline", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Toast.makeText(homeScreenAfterLogin.this, "Please Turn On WIFI", Toast.LENGTH_SHORT).show();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

            return;
        }

        try {
            if (!GPSconnectionStatus.isGPSOn()) {
                // Internet Connection is not present
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Turn on Location Services")
                        .setCancelable(false)
                        .setPositiveButton("Open Settings", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(onGPS);
                            }
                        })
                        .setNegativeButton("Stay Offline", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(homeScreenAfterLogin.this, "Please Turn On WIFI", Toast.LENGTH_SHORT).show();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

                return;
            }
        } catch (Settings.SettingNotFoundException e) {
            Toast.makeText(homeScreenAfterLogin.this, "Cannot Open Settings", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


        if (!checkPermissions()) {
            requestPermissions();
        } else {
            try {
                getLastLocation();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Please Enable location permission", Toast.LENGTH_SHORT).show();

            }
        }
    }

    /**
     * Provides a simple way of getting a device's location and is well suited for
     * applications that do not require a fine-grained location and that do not need location
     * updates. Gets the best and most recent location currently available, which may be null
     * in rare cases when a location is not available.
     * <p>
     * Note: this method should be called after location permission has been granted.
     */

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            // mLatitudeText.setText(String.valueOf(mLastLocation.getLatitude()));


                            latitude = mLastLocation.getLatitude();
                            longitude = mLastLocation.getLongitude();


                            String myLocality = getLocationInString(latitude, longitude);
                            // userLocationInformation userLocation=new userLocationInformation(latitude,longitude,myLocality);
                            // databaseReference.child("User Locations").child(user.getUid()).setValue(userLocation);


                            // mLatitudeText.setText(String.format(Locale.ENGLISH, "%s: %f",
                            ///   mLatitudeLabel,
                            // mLastLocation.getLatitude();
                            //mLongitudeText.setText(String.format(Locale.ENGLISH, "%s: %f",
                            //       mLongitudeLabel,
                            //j       mLastLocation.getLongitude()));

                        } else {
                            // Log.w("This activity", "getLastLocation:exception", task.getException());
                            showSnackbar(getString(R.string.no_location_detected));
                        }
                    }
                });
    }

    /**
     * Shows a {@link Snackbar} using {@code text}.
     *
     * @param text The Snackbar text.
     */
    private void showSnackbar(final String text) {
        View container = findViewById(R.id.homeScreenAfterLogin_container);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(homeScreenAfterLogin.this,
                new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);


    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
          //  Log.i("this activity", "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });

        } else {
           // Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
     //   Log.i(TAG, "onRequestPermissionResult");

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
             //   Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }


    }

    public String getLocationInString(double latitude,double longitude) {
        String myLocality = null;

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

            if (addresses.isEmpty()) {
                Log.i(TAG, "Waiting for Location");
            } else {
                if (addresses.size() > 0) {
                   /* Log.d(TAG, addresses.get(0).getFeatureName() + ", "
                            + addresses.get(0).getLocality() + ","
                            + addresses.get(0).getAdminArea() + ","
                            + addresses.get(0).getCountryName()+","+addresses.get(0).getPostalCode()+","+addresses.get(0).getPremises()+","+addresses.get(0).getSubAdminArea()+","+addresses.get(0).getSubLocality());
                   */

                    String locationDisplay =addresses.get(0).getAddressLine(0);
/*
                            addresses.get(0).getFeatureName() + ", "
                            + addresses.get(0).getLocality() + ","
                            + addresses.get(0).getAdminArea() + ","
                            + addresses.get(0).getCountryName()+","+addresses.get(0).getPostalCode()+","+addresses.get(0).getPremises()+","+addresses.get(0).getSubAdminArea()+
                            ","+addresses.get(0).getSubLocality();
                            */
                    sendMyLocationText=addresses.get(0).getAddressLine(0);
                    pincodeForSeacrhingDB=addresses.get(0).getPostalCode();
                    locationDisplayText.setText(locationDisplay);




 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    id = user.getUid();
                   // DatabaseReference myRef = FirebaseDatabase.getInstance().getReference().child("USERS").child(id).child("PERSONAL_DETAILS");
                    final DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();

                    myRef.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            myRef.child("USERS").child(id).child("PERSONAL_DETAILS").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    FirebasePersonalDetailsEntity uInfo = dataSnapshot.getValue(FirebasePersonalDetailsEntity.class);
                                    getPhoneNumberForFCM=uInfo.getContactno().toString();
                                    Log.i("getPhoneFCM",getPhoneNumberForFCM);
                                    myRef.child("FCM_DB").child(getPhoneNumberForFCM).setValue(FirebaseInstanceId.getInstance().getToken());
                                    Log.i("updated_bro","token");

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });





///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




                    // myLocality=  addresses.get(0).getLocality() ;
                  // System.out.println(hospitalName);
/*
                    databaseReference = FirebaseDatabase.getInstance().getReference();
                    databaseReference.child("Hospitals").child(addresses.get(0).getLocality()).addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            // userInformation3 userInformation=dataSnapshot.getValue(userInformation3.class);
                            String data = userInformation.getLocalityHospitals();
                            hospitalNameDisplayText.setText(data);

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

*/                  //FirebaseUser user= mFirebaseAuth.getInstance().getCurrentUser();
                  //  id=user.getUid();
                   // CollectionReference cities = db.collection("Hospitals 2");
                  //  String query = cities.getPath().toString();

              /*       db = FirebaseFirestore.getInstance();
                    db.collection("Hospitals 2").document("City").collection("Thane").document("Hospital 1").get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if(task.isSuccessful()){
                                DocumentSnapshot documentSnapshot=task.getResult();

                                // Log.i("name",username);
                                flag=1;

                            }else{
                                username="Unknown";
                                int flag= 0;
                            }
                        }
                    }); */




                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return  myLocality;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_screen_after_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_help){
            Intent helpIntent = new Intent(homeScreenAfterLogin.this,homeScreenAfterLogin.class);
            startActivity(helpIntent);
        } else if (id == R.id.nav_my_profile) {
            Intent helpIntent = new Intent(homeScreenAfterLogin.this,myProfile.class);
            startActivity(helpIntent);

        } else if (id == R.id.nav_relatives) {
            Intent helpIntent = new Intent(homeScreenAfterLogin.this,relativeDataDisplay.class);
            startActivity(helpIntent);
        } else if (id == R.id.nav_medical_history) {

        } else if (id == R.id.nav_videos) {
            mAuth.signOut();
            finish();
            startActivity(new Intent(this, firstScreen.class));
        }  else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_about) {
            Intent helpIntent = new Intent(homeScreenAfterLogin.this,About.class);
            startActivity(helpIntent);

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        if(view==victimButton1)
        {
            //open activity to help victim
            Toast.makeText(this, "Victim needs help", Toast.LENGTH_SHORT).show();

        }
        if(view==victimButton2)
        {
            //open activity to help victim
            Toast.makeText(this, "Victim needs help", Toast.LENGTH_SHORT).show();

        }
        if(view==helperButton)
        {
            //open activity for helper
            Toast.makeText(this, "Helper calling for ambulance", Toast.LENGTH_SHORT).show();


            Intent intent = new Intent(homeScreenAfterLogin.this, HospitalsDisplay.class);

            intent.putExtra("latitude",latitude);
            intent.putExtra("longitude",longitude);
            intent.putExtra("MyPincodeForHospitals", pincodeForSeacrhingDB);
            startActivity(intent);

        }

        if(view==victimButton1)
        {
           // sendUserLocationToDatabase();
            //create a trigger flag in the database
            createTriggerInDatabaseForHelp();



            ///Starting a new activity to provide help to the user


            //Sending messages to the reltives
          //  sendRelativesMessageForHelp();

            //Sending location contents to the database





        }
        if(view==victimButton2)
        {
            // sendUserLocationToDatabase();
            //create a trigger flag in the database
            createTriggerInDatabaseForHelp();



            ///Starting a new activity to provide help to the user


            //Sending messages to the reltives
            //  sendRelativesMessageForHelp();

            //Sending location contents to the database





        }

    }

    private void createTriggerInDatabaseForHelp() {

        DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        FirebaseAuth FirebaseAuthentication= FirebaseAuth.getInstance();
        FirebaseUser user =FirebaseAuthentication.getCurrentUser();
        boolean flag= true;
      //  FirebaseEmergencyDetailsEntity firebaseEmergencyDetailsEntity = new FirebaseEmergencyDetailsEntity("TRUE");

        try {

            mDatabaseReference.child("USERS").child(user.getUid()).child("helpFlag").setValue(flag);
            mDatabaseReference.child("USERS").child(user.getUid()).child("location").setValue(sendMyLocationText);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Falied. Please Check Internet Connection", Toast.LENGTH_SHORT).show();

        }
        Intent intent = new Intent(homeScreenAfterLogin.this, VictimHelpProviderTempActivity.class);
        intent.putExtra("MyLocation",sendMyLocationText);
        intent.putExtra("MyPincode", pincodeForSeacrhingDB);
        startActivity(intent);

    }

    private void sendUserLocationToDatabase() {
    }

    private void sendRelativesMessageForHelp() {

        sendSMSMessage();


    }

    protected void sendSMSMessage() {

        Log.i("name2","Completed data snapshot");

        for (int i = 0; i <= 1; i++) {
            phoneNo =  relativeContact[i];
            message = "I NEED HELP ->"+sendMyLocationText;
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(getApplicationContext(), "SMS sent.",
                    Toast.LENGTH_LONG).show();

        }

    }




}
