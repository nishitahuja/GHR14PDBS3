package com.projectdeepblue.goldenhourresponse;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.projectdeepblue.goldenhourresponse.FirebaseHelper.FirebaseFetchHospitalsDetailsEntity;
import com.projectdeepblue.goldenhourresponse.LocationParsers.PlaceDetailsJSONParser;
import com.projectdeepblue.goldenhourresponse.LocationParsers.PlaceJSONParser;
import com.projectdeepblue.goldenhourresponse.RecyclerView.MyAdapter;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HospitalsDisplay extends AppCompatActivity{

    private RecyclerView recyclerView;
    private List<ListItem> listItems;
    private RecyclerView.Adapter adapter;


    GoogleMap mGoogleMap;
    Bundle userLocationDetails;
    Bundle b;

    private FirebaseFirestore db;
    private DatabaseReference myRef;
    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth mFirebaseAuth;
    private String id;
    private long childrenCount;
    private Button makeCall;
    private String hospitalName;
    private String contactno;
    public String address;
    double mLatitude = 0;
    double mLongitude = 0;
    private String pinCode;
    String reference;

    String[] placeReferences = new String[70];
    private int numberOfNearbyPlaces;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospitals_display);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        b=getIntent().getExtras();


        listItems = new ArrayList<>();
        adapter = new MyAdapter(listItems, this);


        mLongitude = getIntent().getDoubleExtra("longitude", 0);
        mLatitude = getIntent().getDoubleExtra("latitude", 0);
        Double.toString(mLongitude);
        Double.toString(mLatitude);


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        id = user.getUid();

        myRef = FirebaseDatabase.getInstance().getReference().child("hospital").child(b.getString("MyPincodeForHospitals"));
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                childrenCount = dataSnapshot.getChildrenCount();
                for (int i = 0; i <= (childrenCount / 3); i++) {
                    final String childKey = dataSnapshot.getKey();
                    Log.i("keyname", childKey);

                    myRef.child(childKey).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            FirebaseFetchHospitalsDetailsEntity data = dataSnapshot.getValue(FirebaseFetchHospitalsDetailsEntity.class);
                            // Log.i("data1",data.getHospitaladdress().toString());
                            // Log.i("data3",data.getHospitalno().toString());
                            // Log.i("keyvalue",childKey);
                            hospitalName = childKey;
                            contactno = data.getHospitalno();
                            address = data.getHospitaladdress();
                            ListItem listItem = new ListItem(
                                    hospitalName, contactno, address

                            );
                            listItems.add(listItem);
                            recyclerView.setAdapter(adapter);


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        String type = "hospital";
        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
       sb.append("location=" + mLatitude + "," + mLongitude);
       // sb.append("location=" + "19.225134" + "," + "72.9772597");
        sb.append("&radius=500");
        sb.append("&types=hospital");
        sb.append("&sensor=true");
        sb.append("&key=AIzaSyDP-Gf3j2mlODaurV_20MmTiTSF3wBYsQY");

        //   StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=19.202129,72.978070&radius=5000&types=hospital&sensor=true&key=AIzaSyAy9J8LwJWQpsoGbnmAjm-wJ5FBWmUw19Y");
        // Creating a new non-ui thread task to download Google place json data
         Log.i("sb",sb.toString());

        PlacesTask2 placesTask2 = new PlacesTask2();

        // Invokes the "doInBackground()" method of the class PlaceTask
        placesTask2.execute(sb.toString());

        // Getting place reference from the map

        //  Log.i("number",Integer.toString(numberOfNearbyPlaces));


    }


    /**
     * A method to download json data from url
     */
    private String downloadUrl2(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            //  Log.i("url","url conection");

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            // Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }



    /** A class, to download Google Places */
    private class PlacesTask2 extends AsyncTask<String, Integer, String> {



        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {

            Log.i("doInBackground","thread begins");
            try{
                data = downloadUrl2(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result){
            ParserTask2 parserTask = new ParserTask2();

            // Start parsing the Google places in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }

    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask2 extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

        JSONObject jObject;

        // Invoked by execute() method of this object
        @Override
        protected List<HashMap<String,String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                /** Getting the parsed data as a List construct */
                places = placeJsonParser.parse(jObject);

            }catch(Exception e){
              //  Log.d("Exception",e.toString());
            }
            return places;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(List<HashMap<String,String>> list){

            // Clears all the existing markers
            // mGoogleMap.clear();


            numberOfNearbyPlaces=list.size();
            for(int i=0;i<list.size();i++){
                // Getting a place from the places list
                HashMap<String, String> hmPlace = list.get(i);

                // Getting latitude of the place
                double lat = Double.parseDouble(hmPlace.get("lat"));

                // Getting longitude of the place
                double lng = Double.parseDouble(hmPlace.get("lng"));

                // Getting name
                String name = hmPlace.get("place_name");

                // Getting vicinity
                String vicinity = hmPlace.get("vicinity");

                LatLng latLng = new LatLng(lat, lng);

                // Setting the title for the marker.
                //This will be displayed on taping the marker
                // markerOptions.title(name + " : " + vicinity);

                //****************************************************
                //       Need to create array to store the reference
                //***************************************************8


                placeReferences[i]=hmPlace.get("reference");

                Log.i("references",placeReferences[i]);

                // Linking Marker id and place reference
                // mMarkerPlaceLink.put(m.getId(), hmPlace.get("reference"));


            }
            for(int i=0;i<5;i++) {
                // Log.i("reference"+i, reference);
                reference = placeReferences[i];
              //  Log.i("references2",reference);
                StringBuilder sb2 = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?");
                sb2.append("reference=" + reference);
                sb2.append("&sensor=true");
                sb2.append("&key=AIzaSyAy9J8LwJWQpsoGbnmAjm-wJ5FBWmUw19Y");


                // Creating a new non-ui thread task to download Google place details
                PlacesTask placesTask = new PlacesTask();

                // Invokes the "doInBackground()" method of the class PlaceTask
                placesTask.execute(sb2.toString());


                /** A method to download json data from url */
            }

        }

    }


















    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);


            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        }catch(Exception e){
            Log.d("Exception", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }

    /** A class, to download Google Place Details */
    private class PlacesTask extends AsyncTask<String, Integer, String>{

        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try{
                data = downloadUrl(url[0]);
            }catch(Exception e){
               // Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result){
            ParserTask parserTask = new ParserTask();

            // Start parsing the Google place details in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Place Details in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, HashMap<String,String>> {

        JSONObject jObject;

        // Invoked by execute() method of this object
        @Override
        protected HashMap<String,String> doInBackground(String... jsonData) {

            HashMap<String, String> hPlaceDetails = null;
            PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                // Start parsing Google place details in JSON format
                hPlaceDetails = placeDetailsJsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return hPlaceDetails;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(HashMap<String,String> hPlaceDetails) {

            try {


                String name = hPlaceDetails.get("name");
                String icon = hPlaceDetails.get("icon");
                String vicinity = hPlaceDetails.get("vicinity");
                String lat = hPlaceDetails.get("lat");
                String lng = hPlaceDetails.get("lng");
                String formatted_address = hPlaceDetails.get("formatted_address");
                String formatted_phone = hPlaceDetails.get("formatted_phone");
                String website = hPlaceDetails.get("website");
                String rating = hPlaceDetails.get("rating");
                String international_phone_number = hPlaceDetails.get("international_phone_number");
                String url = hPlaceDetails.get("url");
                //  text1.setText(name);
                // text2.setText(formatted_phone);
                Log.i("name", name);
                Log.i("contact", formatted_phone);
                Log.i("address", formatted_address);

                hospitalName = name;
                contactno = formatted_phone;
                address = formatted_address;
                ListItem listItem = new ListItem(
                        hospitalName, contactno, address

                );
                listItems.add(listItem);
                recyclerView.setAdapter(adapter);
                String mimeType = "text/html";
                String encoding = "utf-8";

                String data = "<html>" +
                        "<body><img style='float:left' src=" + icon + " /><h1><center>" + name + "</center></h1>" +
                        "<br style='clear:both' />" +
                        "<hr  />" +
                        "<p>Vicinity : " + vicinity + "</p>" +
                        "<p>Location : " + lat + "," + lng + "</p>" +
                        "<p>Address : " + formatted_address + "</p>" +
                        "<p>Phone : " + formatted_phone + "</p>" +
                        "<p>Website : " + website + "</p>" +
                        "<p>Rating : " + rating + "</p>" +
                        "<p>International Phone  : " + international_phone_number + "</p>" +
                        "<p>URL  : <a href='" + url + "'>" + url + "</p>" +
                        "</body></html>";
            }catch (Exception e){

            }
                // Setting the data in WebView
                //     mWvPlaceDetails.loadDataWithBaseURL("", data, mimeType, encoding, "");
            }


    }

}


