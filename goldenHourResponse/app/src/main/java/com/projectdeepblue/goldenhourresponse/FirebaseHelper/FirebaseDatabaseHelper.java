package com.projectdeepblue.goldenhourresponse.FirebaseHelper;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Omkar on 01-02-2018.
 */

public class FirebaseDatabaseHelper {

    Context context;
    String status;
    String SNAPSHOTEXISTS;
    private static final String TAG = FirebaseDatabaseHelper.class.getSimpleName();
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //              database
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private DatabaseReference databaseReference;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //              authentication
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private FirebaseAuth mAuth;
   // private

    public FirebaseDatabaseHelper() {
        databaseReference = FirebaseDatabase.getInstance().getReference();


    }

    public String isUserLoggedIn()
    {


        FirebaseAuth.AuthStateListener mAuthListener;
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
                if(user!=null){
                    status="USER_SIGNED_IN";
                }else{
                   status="USER_NOT_SIGNED_IN";
                }
            }
        };
        return status;
    }

    public void addPersonalDetails(Bundle userPersonalInformation)
    {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mDatabaseReference = database.getReference();

        FirebaseAuth FirebaseAuthentication= FirebaseAuth.getInstance();
        FirebaseUser user =FirebaseAuthentication.getCurrentUser();

        FirebasePersonalDetailsEntity personalDetailsEntityObject = new FirebasePersonalDetailsEntity(userPersonalInformation);
        if(isUserLoggedIn()=="USER_SIGNED_IN")
        {
            try {
             //   mDatabaseReference = FirebaseDatabase.getInstance().getReference();
                mDatabaseReference.child("USERS").child(user.getUid()).child("PERSONAL_DETAILS").setValue(personalDetailsEntityObject);
                status = "UPDATED";
                Log.i("tag","updated bro");
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(context, "hagg diya", Toast.LENGTH_SHORT).show();
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // put some code to execute instead of forcing the application to force shutdown
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            }
        }
        else
        {
                status= "DATABASE UPDATE FAILED";
        }

    }

    public void addRelativeDetails(Bundle userRelativeInformation)
    {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mDatabaseReference = database.getReference();

        FirebaseAuth FirebaseAuthentication= FirebaseAuth.getInstance();
        FirebaseUser user =FirebaseAuthentication.getCurrentUser();
        FirebasePersonalDetailsEntity relativeDetailsEntityObject = new FirebasePersonalDetailsEntity(userRelativeInformation);
        if(isUserLoggedIn()=="USER_SIGNED_IN")
        {
            try {
              //  mDatabaseReference = FirebaseDatabase.getInstance().getReference();
                mDatabaseReference.child("USERS").child(user.getUid()).child("RELATIVE_DETAILS").setValue(relativeDetailsEntityObject);
                status = "UPDATED";
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(context, "hagg diya", Toast.LENGTH_SHORT).show();
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // put some code to execute instead of forcing the application to force shutdown
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            }
        }
        else
        {
            status= "DATABASE UPDATE FAILED";
        }

    }




    public String checkIfDetailsAreEntered()
    {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mDatabaseReference = database.getReference();

        FirebaseAuth FirebaseAuthentication= FirebaseAuth.getInstance();
        FirebaseUser user =FirebaseAuthentication.getCurrentUser();
        if(isUserLoggedIn()=="USER_SIGNED_IN")
        {
            try {
              //  DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
                databaseReference.child("USERS").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists())
                        {
                            SNAPSHOTEXISTS ="FORM_FILLED";
                        }
                        else
                        {
                            SNAPSHOTEXISTS="FORM_NOT_FILLED";
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
            catch (Exception e)
            {
                e.printStackTrace();
                SNAPSHOTEXISTS="FORM_NOT_FILLED";
                Toast.makeText(context, "hagg diya", Toast.LENGTH_SHORT).show();
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // put some code to execute instead of forcing the application to force shutdown
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            }
        }
        else
        {
            SNAPSHOTEXISTS= "USER_NOT_SIGNED_IN";
        }

        return SNAPSHOTEXISTS;
    }


}
