package com.projectdeepblue.goldenhourresponse.FirebaseHelper;

/**
 * Created by Omkar on 07-02-2018.
 */

public class FirebaseEmergencyDetailsEntity {

    public String flag;

    public FirebaseEmergencyDetailsEntity(String flag) {
        this.flag = flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public FirebaseEmergencyDetailsEntity() {
    }
}
