package com.projectdeepblue.goldenhourresponse.Firestore;

import android.os.Bundle;

/**
 * Created by Omkar on 19-01-2018.
 */

public class personalDetailsEntity {
     String firstName;
    String lastName;
     String  contactno;
     Integer age;
     String gender;
     String bloodGroup;
     String disease;

    public personalDetailsEntity() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getContactno() {
        return contactno;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public String getDisease() {
        return disease;
    }

    public personalDetailsEntity(Bundle b) {
        this.firstName = b.getString("FIRST_NAME") ;
        this.lastName = b.getString("LAST_NAME") ;
        this.contactno =  b.getString("CONTACT") ;
        this.age =  Integer.parseInt(b.getString("AGE"));
        this.disease=b.getString("DISEASE");
        this.gender=b.getString("GENDER");
        this.bloodGroup=b.getString("BLOODGROUP");




    }
}
