package com.projectdeepblue.goldenhourresponse;

/**
 * Created by Omkar on 13-01-2018.
 */

public class ListItem {
    private String head;
    private String desc;
    private String addr;

    public ListItem(String head, String desc,String addr) {
        this.head = head;
        this.desc = desc;
        this.addr=addr;
    }


    public String getHead() {
        return head;
    }

    public String getDesc() {
        return desc;
    }

    public String getAddr() {
        return addr;
    }
}