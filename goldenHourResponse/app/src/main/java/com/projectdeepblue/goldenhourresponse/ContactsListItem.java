package com.projectdeepblue.goldenhourresponse;

/**
 * Created by Omkar on 22-01-2018.
 */

public class ContactsListItem {

    private String head;
    private String desc;

    public ContactsListItem(String head, String desc) {
        this.head = head;
        this.desc = desc;
    }

    public String getHead() {
        return head;
    }

    public String getDesc() {
        return desc;
    }
}
