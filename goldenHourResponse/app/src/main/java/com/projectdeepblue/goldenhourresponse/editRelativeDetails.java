package com.projectdeepblue.goldenhourresponse;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.projectdeepblue.goldenhourresponse.FirebaseHelper.FirebaseRelativeDetailsEntity;
import com.projectdeepblue.goldenhourresponse.RecyclerView.EditRelativeDetailsAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class editRelativeDetails extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_READ_CONTACTS = 444;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final String DEFAULT ="-NA-" ;


    boolean contactsExceeded;
    private Button saveAndProceed;
    private Button selectContacts,goBack;
    private  String[] relativeName= new  String[7];
    private  String[] relativeContact= new  String[7];

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    int ad;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////








    private RecyclerView recyclerView;
    private List<editRelativeDetailsList> contactsList;
    private  RecyclerView.Adapter adapter;
    private int i=0;
    Map<Object,String> mMap=new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_relative_details);


        recyclerView=(RecyclerView)findViewById(R.id.recyclerViewEditRelativeContacts);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        contactsList=new ArrayList<>();
        adapter=  new EditRelativeDetailsAdapter(contactsList,this);


        SharedPreferences sharedPreferences=getSharedPreferences("RelativeData", Context.MODE_PRIVATE);

        for (int i = 0; i <( sharedPreferences.getAll().size())/2; i++) {

            relativeName[i]=sharedPreferences.getString("relativeName"+i,DEFAULT);
            relativeContact[i]=sharedPreferences.getString("relativeNumber"+i,DEFAULT);

            editRelativeDetailsList listItem = new  editRelativeDetailsList(
                    relativeName[i], relativeContact[i]

            );

            contactsList.add(listItem);

        }
        recyclerView.setAdapter(adapter);


        //  firstRelativeName=(EditText)findViewById(R.id.editText_FirstRelativeFullName);
        // secondRelativeName=(EditText)findViewById(R.id.editText_secondRelativeFullName);
        // firstRelativeContactno=(EditText)findViewById(R.id.R1Contact);
        //secondRelativeContact=(EditText)findViewById(R.id.R2contact);

        saveAndProceed = (Button)findViewById(R.id.Button_update_complete);
        selectContacts = (Button)findViewById(R.id.Button_select_contacts_edit_relative);
        goBack=(Button)findViewById(R.id.Button_editRelative_back);

        saveAndProceed.setOnClickListener(this);
        selectContacts.setOnClickListener(this);
        goBack.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        if(view==saveAndProceed)
        {
            if(contactsExceeded==true)
            {
                contactsExceeded=false;
                Toast.makeText(this, "Please select only 5 contacts", Toast.LENGTH_SHORT).show();
            }
            else{
                registerUser2();

            }
        }
        if(view==selectContacts){
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivityForResult(intent, 1);
        }
        if(view==goBack)
        {
            Intent homeIntent = new Intent(editRelativeDetails.this,homeScreenAfterLogin.class);
            startActivity(homeIntent);
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //                              need to add request permission

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onActivityResult( ,Activity.RESULT_OK,);
            }
        }
    }
    */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {





        if (resultCode == Activity.RESULT_OK) {
            Uri contactData = data.getData();
            Cursor c =  getContentResolver().query(contactData, null, null, null, null);
            if (c.moveToFirst()) {

                String phoneNumber="",emailAddress="";
                String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                if ( hasPhone.equalsIgnoreCase("1"))
                    hasPhone = "true";
                else
                    hasPhone = "false" ;

                if (Boolean.parseBoolean(hasPhone))
                {
                    Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ contactId,null, null);
                    while (phones.moveToNext())
                    {
                        phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }
                    phones.close();
                }

                // Find Email Addresses
                Cursor emails = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,null,ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId,null, null);
                while (emails.moveToNext())
                {
                    emailAddress = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                }
                emails.close();

                //mainActivity.onBackPressed();
                // Toast.makeText(mainactivity, "go go go", Toast.LENGTH_SHORT).show();



                editRelativeDetailsList listItem= new editRelativeDetailsList(
                        name,phoneNumber);

                contactsList.add(listItem);
                //  userDetials.putString("RELATIVE_NAME"+i,name);

                //  userDetials.putString("RELATIVE_CONTACT"+i,phoneNumber);
                i++;
            }
            recyclerView.setAdapter(adapter);
            if(adapter.getItemCount()>5)
            {
                Toast.makeText(this, "5 Relatives Selected Already", Toast.LENGTH_SHORT).show();
                contactsExceeded=true;
            }
            else {
                contactsExceeded=false;
            }
            c.close();
        }
    }


    private void registerUser2() {

        Bundle userDetials=new Bundle();
        SharedPreferences sharedPreferences=getSharedPreferences("RelativeData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        sharedPreferences.edit().clear().commit();


        for(int i=0;i <adapter.getItemCount();i++){
            //  Log.i("RELATIVE_NAME"+i,contactsList.get(i).getHead());
            //  Log.i("RELATIVE_CONTACT"+i,contactsList.get(i).getDesc());
            editor.putString("relativeName"+i,contactsList.get(i).getHead());
            char[] cno=contactsList.get(i).getDesc().toCharArray();
            char[] temp=new char[10];
            int k=9;
            int j=cno.length-1;
            while(k>=0){
                temp[k]=cno[j];
                k--;
                j--;
            }
            String contactno=String.valueOf(temp);
            Log.i("contactno:",contactno);
            editor.putString("relativeNumber"+i,contactno);
            editor.commit();
            userDetials.putString("RELATIVE_NAME"+i,contactsList.get(i).getHead());
            userDetials.putString("RELATIVE_CONTACT"+i,contactno);
        }

        Intent intent=new Intent(editRelativeDetails.this,homeScreenAfterLogin.class);

        //  userDetials.putString("FIRST_RELATIVE_NAME","Omkar Tulaskar");
        //   userDetials.putString("SECOND_RELATIVE_NAME","helo2");
        //   userDetials.putString("FIRST_RELATIVE_CONTACT","44");
        //  userDetials.putString("SECOND_RELATIVE_CONTACT","95544");

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mDatabaseReference = database.getReference();

        FirebaseAuth FirebaseAuthentication= FirebaseAuth.getInstance();
        FirebaseUser user =FirebaseAuthentication.getCurrentUser();


        FirebaseRelativeDetailsEntity relativeDetailsEntityObject= new FirebaseRelativeDetailsEntity(userDetials);

        try {
            //   mDatabaseReference = FirebaseDatabase.getInstance().getReference();

            for(int i=0;i<=adapter.getItemCount();i++) {
                mDatabaseReference.child("RELATIVE_DB").child(user.getUid()).child("relativeName" + i).setValue(userDetials.getString("RELATIVE_NAME" + i));
                mDatabaseReference.child("RELATIVE_DB").child(user.getUid()).child("relativeNumber" + i).setValue(userDetials.getString("RELATIVE_CONTACT" + i));
                //mDatabaseReference.child("RELATIVE_DB").child(user.getUid()).setValue(relativeDetailsEntityObject);
            }
            Log.i("tag","updated bro");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Sorry There was problem updating details." +
                    "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // put some code to execute instead of forcing the application to force shutdown
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }



        startActivity(intent);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
    @Override
    protected void onStart() {

        super.onStart();
        if(!checkContactsPermissions())
        {
            requestContactsPermissions();
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Contact Permissions

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    private boolean checkContactsPermissions(){
        int permissionState = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_CONTACTS);
        return permissionState == PackageManager.PERMISSION_GRANTED;

    }

    private void requestContactsPermissions()
    {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.READ_CONTACTS);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            //  Log.i("this activity", "Displaying permission rationale to provide additional context.");
            // Request permission
            startContactsPermissionRequest();
        } else {
            // Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startContactsPermissionRequest();
        }
    }


    private void startContactsPermissionRequest()
    {
        ActivityCompat.requestPermissions(editRelativeDetails.this,
                new String[]{android.Manifest.permission.READ_CONTACTS},
                REQUEST_PERMISSIONS_REQUEST_CODE);

    }



}
