package com.projectdeepblue.goldenhourresponse.RecyclerView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.projectdeepblue.goldenhourresponse.R;
import com.projectdeepblue.goldenhourresponse.editRelativeDetailsList;

import java.util.List;

/**
 * Created by Omkar on 08-02-2018.
 */
public class EditRelativeDetailsAdapter extends RecyclerView.Adapter<EditRelativeDetailsAdapter.ViewHolder> {

    private List<editRelativeDetailsList> listItems;
    private Context context;

    public EditRelativeDetailsAdapter(List<editRelativeDetailsList> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @Override
    public EditRelativeDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_edit_relative_contacts,parent,false);
        return  new EditRelativeDetailsAdapter.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final EditRelativeDetailsAdapter.ViewHolder holder, final int position) {

        editRelativeDetailsList listItem= listItems.get(position);

        holder.head.setText(listItem.getHead().toString());
        holder.desc.setText(listItem.getDesc().toString());
        holder.head.setText(listItem.getHead().toString());
        holder.desc.setText(listItem.getDesc().toString());
        holder.EditRelativecancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listItems.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, listItems.size());
                //holder.itemView.setVisibility(View.GONE);
            }
        });


    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView head;
        public TextView desc;
        public Button EditRelativecancel;


        public ViewHolder(View itemView) {
            super(itemView);

            head=(TextView)itemView.findViewById(R.id.TextViewEditRelativeContactsName);
            desc=(TextView)itemView.findViewById(R.id.TextViewEditRelativeContactNumber);
            EditRelativecancel=(Button)itemView.findViewById(R.id.Button_editRelativeCancelContact);
        }
    }

}
