package com.projectdeepblue.goldenhourresponse;

/**
 * Created by Omkar on 06-02-2018.
 */

public class RelativeListItem {

    private String head;
    private String desc;

    public RelativeListItem(String head, String desc) {
        this.head = head;
        this.desc = desc;
    }

    public String getHead() {
        return head;
    }

    public String getDesc() {
        return desc;
    }
}
