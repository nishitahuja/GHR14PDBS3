package com.projectdeepblue.goldenhourresponse.RecyclerView;

/**
 * Created by Omkar on 05-02-2018.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.projectdeepblue.goldenhourresponse.R;
import com.projectdeepblue.goldenhourresponse.RelativeListItem;

import java.util.List;



public class RelativesDisplayAdapter extends RecyclerView.Adapter<RelativesDisplayAdapter.ViewHolder> {

    private List<RelativeListItem> listItems;
    private Context context;

    public RelativesDisplayAdapter(List<RelativeListItem> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @Override
    public RelativesDisplayAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_relatives_display,parent,false);
        return  new RelativesDisplayAdapter.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final RelativesDisplayAdapter.ViewHolder holder, final int position) {

        RelativeListItem listItem= listItems.get(position);

        holder.head.setText(listItem.getHead().toString());
        holder.desc.setText(listItem.getDesc().toString());


    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView head;
        public TextView desc;


        public ViewHolder(View itemView) {
            super(itemView);

            head=(TextView)itemView.findViewById(R.id.TextViewRelativeContactsName);
            desc=(TextView)itemView.findViewById(R.id.TextViewRelativeContactNumber);

        }
    }

}
