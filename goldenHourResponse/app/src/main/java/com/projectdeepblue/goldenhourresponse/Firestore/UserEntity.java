package com.projectdeepblue.goldenhourresponse.Firestore;

/**
 * Created by Omkar on 03-01-2018.
 */

public class UserEntity {
    public String name;
    String  contactno;
    public String completeAddress;
    public Integer age;
    public String flag;

    public UserEntity(String flag,String name,String contactno,String completeAddress, Integer age) {
        this.completeAddress = completeAddress;
        this.age = age;
        this.name=name;
        this.contactno=contactno;
        this.flag=flag;
    }

    public String getName() {
        return name;
    }

    public String getContactno() {
        return contactno;
    }

    public String getCompleteAddress() {
        return completeAddress;
    }

    public Integer getAge() {
        return age;
    }

    public String getFlag() {
        return flag;
    }




//default contructor for required for database calls

    public UserEntity()
    {

    }


    public UserEntity(String name, String completeAddress, String contactno,Integer age,String flag) {
        this.name = name;
        this.completeAddress = completeAddress;
        this.contactno = contactno;
        this.age=age;
        this.flag=flag;
    }
    public UserEntity(String name, String contactno,Integer age) {
        this.name = name;

        this.contactno = contactno;
        this.age=age;

    }

}


