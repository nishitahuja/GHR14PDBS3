package com.projectdeepblue.goldenhourresponse;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.projectdeepblue.goldenhourresponse.FirebaseHelper.FirebaseFetchHospitalsDetailsEntity;

public class testing1 extends AppCompatActivity {
    private DatabaseReference myRef;
    private String id;
    private long childrenCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing1);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        id = user.getUid();

        myRef = FirebaseDatabase.getInstance().getReference().child("hospital").child("400602");
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
               childrenCount=dataSnapshot.getChildrenCount();
               for(int i = 0;i<=(childrenCount/3);i++) {
                   final String childKey = dataSnapshot.getKey();
                 //  Log.i("keyname", childKey);

                   myRef.child(childKey).addValueEventListener(new ValueEventListener() {
                       @Override
                       public void onDataChange(DataSnapshot dataSnapshot) {
                           FirebaseFetchHospitalsDetailsEntity data=dataSnapshot.getValue(FirebaseFetchHospitalsDetailsEntity.class);
                            Log.i("data1",data.getHospitaladdress().toString());
                           Log.i("data3",data.getHospitalno().toString());
                           Log.i("keyvalue",childKey);

                       }

                       @Override
                       public void onCancelled(DatabaseError databaseError) {

                       }
                   });


               }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }
}
