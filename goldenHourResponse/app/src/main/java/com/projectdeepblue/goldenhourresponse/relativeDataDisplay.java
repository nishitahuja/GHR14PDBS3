package com.projectdeepblue.goldenhourresponse;

/**
 * Created by Omkar on 05-02-2018.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.projectdeepblue.goldenhourresponse.RecyclerView.RelativesDisplayAdapter;

import java.util.ArrayList;
import java.util.List;

public class relativeDataDisplay extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private static final String DEFAULT ="-NA-" ;
    private  String[] relativeName= new  String[7];
    private  String[] relativeContact= new  String[7];
    private long childrenCount;
    private RecyclerView recyclerView;
    private List<RelativeListItem> listItems;
    private RecyclerView.Adapter adapter;

    private DatabaseReference myRef;
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private FirebaseAuth mFirebaseAuth;
    private String id;
    private Button editData;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative_data_display);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewRelative);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        listItems = new ArrayList<>();
        adapter = new RelativesDisplayAdapter(listItems, this);
        editData=(Button)findViewById(R.id.Button_edit_relative_details) ;
        editData.setOnClickListener(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

       SharedPreferences sharedPreferences=getSharedPreferences("RelativeData", Context.MODE_PRIVATE);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        id = user.getUid();


        for (int i = 0; i <( sharedPreferences.getAll().size())/2; i++) {

            relativeName[i]=sharedPreferences.getString("relativeName"+i,DEFAULT);
            relativeContact[i]=sharedPreferences.getString("relativeNumber"+i,DEFAULT);

            RelativeListItem listItem = new RelativeListItem(
                    relativeName[i], relativeContact[i]

            );

            listItems.add(listItem);
            recyclerView.setAdapter(adapter);
        }
    }







    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_screen_after_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_help){
            Intent helpIntent = new Intent(relativeDataDisplay.this,homeScreenAfterLogin.class);
            startActivity(helpIntent);
        } else if (id == R.id.nav_my_profile) {
            Intent helpIntent = new Intent(relativeDataDisplay.this,myProfile.class);
            startActivity(helpIntent);

        } else if (id == R.id.nav_relatives) {
            Intent helpIntent = new Intent(relativeDataDisplay.this,relativeDataDisplay.class);
            startActivity(helpIntent);

        } else if (id == R.id.nav_medical_history) {

        } else if (id == R.id.nav_videos) {

        }   else if (id == R.id.nav_share) {


        } else if (id == R.id.nav_about) {
            Intent helpIntent = new Intent(relativeDataDisplay.this,About.class);
            startActivity(helpIntent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        if(view==editData)
        {
            Intent helpIntent = new Intent(relativeDataDisplay.this,editRelativeDetails.class);
            startActivity(helpIntent);
        }
    }
}
