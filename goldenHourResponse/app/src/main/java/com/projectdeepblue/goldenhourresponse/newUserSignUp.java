package com.projectdeepblue.goldenhourresponse;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class newUserSignUp extends AppCompatActivity implements View.OnClickListener {

    EditText Pass1,Pass2,Email;
    private Button createNewUserAccount;
    public ProgressDialog mProgressDialog;
    private FirebaseAuth mFirebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user_sign_up);



        mFirebaseAuth=FirebaseAuth.getInstance();

        Pass1=(EditText)findViewById(R.id.EditText_signUp_pass1);
        Pass2=(EditText)findViewById(R.id.EditText_signUp_pass2);

        Email=(EditText)findViewById(R.id.EditText_signUp_Email);

        createNewUserAccount=(Button)findViewById(R.id.Button_create_account);

        createNewUserAccount.setOnClickListener(this);

    }

    private void registeruser1() {
      //  showProgressDialog();

        String pass1=Pass1.getText().toString().trim();
        String pass2=Pass2.getText().toString().trim();
        String email=Email.getText().toString().trim();


        if(email.isEmpty()){

            Email.setError("Must be filled");
            Email.requestFocus();
            return;
        }


        if(pass1.isEmpty()){
            Pass1.setError("Must be filled");
            Pass1.requestFocus();
            return;
        }
        if(pass2.isEmpty()){
            Pass2.setError("Must be filled");
            Pass2.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Email.setError("invalid email");
            Email.requestFocus();
            return;

        }
        if(pass1.length()<6){
            Pass1.setError("enter longer password");
            Pass1.requestFocus();
            return;
        }
        if(!pass1.contentEquals(pass2)) {
            Pass2.setError("pass doesn't match");
            Pass2.requestFocus();
            return;
        }

        mFirebaseAuth.createUserWithEmailAndPassword(email,pass1)
                .addOnCompleteListener(newUserSignUp.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            Toast.makeText(newUserSignUp.this, "User Register Succesfully", Toast.LENGTH_SHORT).show();
                           // hideProgressDialog();
                            Intent intent = new Intent(newUserSignUp.this, userSignUpForm_1.class);
                            startActivity(intent);
                            finish();

                        }
                        else
                        {
                            //Toast.makeText(newUserSignUp.this, "Registration Failed", Toast.LENGTH_SHORT).show();
                            Toast.makeText(newUserSignUp.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(newUserSignUp.this, newUserSignUp.class);
                            startActivity(intent);
                            finish();
                          //  hideProgressDialog();

                        }
                    }
                });




    }


    @Override
    public void onClick(View view) {
        if(view==createNewUserAccount)
        {
            registeruser1();
        }
        }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Signing In");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }



}
