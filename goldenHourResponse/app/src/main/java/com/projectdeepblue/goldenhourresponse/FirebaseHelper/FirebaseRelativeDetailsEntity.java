package com.projectdeepblue.goldenhourresponse.FirebaseHelper;

import android.os.Bundle;

/**
 * Created by Omkar on 02-02-2018.
 */

public class FirebaseRelativeDetailsEntity {
    public String relativeName1;
    public String relativeNumber1;
    public String relativeName2;
    public String relativeNumber2;

    public FirebaseRelativeDetailsEntity() {
    }

    public FirebaseRelativeDetailsEntity(Bundle b) {
        this.relativeName1 = b.getString("RELATIVE_NAME0");
        this.relativeNumber1 =b.getString("RELATIVE_CONTACT0");
        this.relativeName2 = b.getString("RELATIVE_NAME1");
        this.relativeNumber2 =b.getString("RELATIVE_CONTACT1");
    }

    public String getRelativeName1() {
        return relativeName1;
    }

    public void setRelativeName1(String relativeName1) {
        this.relativeName1 = relativeName1;
    }

    public String getRelativeNumber1() {
        return relativeNumber1;
    }

    public void setRelativeNumber1(String relativeNumber1) {
        this.relativeNumber1 = relativeNumber1;
    }

    public String getRelativeName2() {
        return relativeName2;
    }

    public void setRelativeName2(String relativeName2) {
        this.relativeName2 = relativeName2;
    }

    public String getRelativeNumber2() {
        return relativeNumber2;
    }

    public void setRelativeNumber2(String relativeNumber2) {
        this.relativeNumber2 = relativeNumber2;
    }
}
