package com.projectdeepblue.goldenhourresponse;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.projectdeepblue.goldenhourresponse.Firestore.FirestoreAddUserDataHelper;

import java.util.HashMap;
import java.util.Map;

public class userSignUpForm_2 extends AppCompatActivity implements View.OnClickListener {


    private EditText completeUserAddress,userAge,userName,userContactno;
    private Spinner diseaseUser,bloodGroupUser,genderUser;
    private Button updateDetails,goToAPP ;
    Map<String,Object> userDataMap=new HashMap<>();
    private String age;
    private String address;

    private String name;
    Integer  contactno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_sign_up_form_2);
       // userDataMap=
        completeUserAddress= (EditText)findViewById(R.id.editText_Address);
        userAge=(EditText)findViewById(R.id.editText_Age);
        userName=(EditText)findViewById(R.id.editText_name);
        userContactno=(EditText)findViewById(R.id.editText_contactno);








        //address=completeUserAddress.getText().toString().trim();
        //age=userAge.getText().toString();


        updateDetails=(Button)findViewById(R.id.Button_moveNext);
        goToAPP=(Button)findViewById(R.id.button_gotToApp);



        updateDetails.setOnClickListener(this);
        goToAPP.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        if (view==updateDetails)
        {
            Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
         //   Log.d("click","click");
          //  userDataMap.put()
            userDataMap.put("userName",userName.getText());
            userDataMap.put("userContactno",userContactno.getText());
            userDataMap.put("userAge",userAge.getText());
         //   userDataMap.put("diseaseUser",diseaseUser.toString());
            userDataMap.put("completeUserAddress",completeUserAddress.getText());
          //  userDataMap.put("bloodGroupUser",bloodGroupUser.toString());
          //  userDataMap.put("genderUser",genderUser.toString());

          FirestoreAddUserDataHelper userDetails=new FirestoreAddUserDataHelper();
            String age2 = userDataMap.get("userAge").toString();
           // Log.d("userDataMap",userDataMap.toString());
            userDetails.updateDetails(userDataMap);
        }

        if(view==goToAPP)
        {
            startActivity(new Intent(userSignUpForm_2.this, homeScreenAfterLogin.class));
        }
    }
}
