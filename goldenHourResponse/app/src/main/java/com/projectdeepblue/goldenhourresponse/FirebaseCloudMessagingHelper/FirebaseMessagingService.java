package com.projectdeepblue.goldenhourresponse.FirebaseCloudMessagingHelper;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;
import com.projectdeepblue.goldenhourresponse.R;
import com.projectdeepblue.goldenhourresponse.homeScreenAfterLogin;

import static android.app.NotificationChannel.DEFAULT_CHANNEL_ID;

/**
 * Created by Omkar on 08-02-2018.
 */
public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Intent intent=new Intent(this,homeScreenAfterLogin.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent=PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this,DEFAULT_CHANNEL_ID )
                 .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Emergency Notification").setAutoCancel(true)
                        .setContentIntent(pendingIntent)
                .setContentText(remoteMessage.getNotification().getBody().toString());
        NotificationManager notificationManager=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0,mBuilder.build());



/*

        try {
            Log.d(TAG, "From: " + remoteMessage.getFrom());
            Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody().toString());
            Log.d(TAG,"DATA:"+remoteMessage.getData().toString());
        }catch (Exception e)
        {
            Log.i("Exception","no body");
        }
*/
        //https://www.androidhive.info/2012/10/android-push-notifications-using-google-cloud-messaging-gcm-php-and-mysql/


    }
}







