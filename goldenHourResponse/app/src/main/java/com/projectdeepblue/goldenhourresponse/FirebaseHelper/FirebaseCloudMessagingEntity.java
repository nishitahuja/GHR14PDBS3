package com.projectdeepblue.goldenhourresponse.FirebaseHelper;

/**
 * Created by Omkar on 08-02-2018.
 */

public class FirebaseCloudMessagingEntity {
    public String token;

    public FirebaseCloudMessagingEntity(String token) {
        this.token = token;
    }

    public FirebaseCloudMessagingEntity() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
