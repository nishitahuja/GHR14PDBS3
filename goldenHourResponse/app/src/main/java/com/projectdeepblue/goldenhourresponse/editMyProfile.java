package com.projectdeepblue.goldenhourresponse;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.projectdeepblue.goldenhourresponse.FirebaseHelper.FirebasePersonalDetailsEntity;

public class editMyProfile extends AppCompatActivity implements View.OnClickListener {
    private EditText firstNameUpdate,lastNameUpdate,contactUpdate,ageUpdate;
    private Spinner genderUpdate,bloodGroupUpdate,diseaseUpdate;
    private String selectedDisease;
    private String selectedGender;
    private String selectedBloodGroup;
    private Button update;
    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_my_profile);

        firstNameUpdate =(EditText)findViewById(R.id.UEditText_FirstName);
        lastNameUpdate =(EditText)findViewById(R.id.UEditText_LastName);
        contactUpdate =(EditText)findViewById(R.id.UEditText_Contact);
        ageUpdate =(EditText)findViewById(R.id.UEditText_age);

        genderUpdate=(Spinner)findViewById(R.id.Uspinner_Gender);
        bloodGroupUpdate=(Spinner)findViewById(R.id.Uspinner_Bloodgroup);
        diseaseUpdate=(Spinner)findViewById(R.id.Uspinner_disease);
        update=(Button)findViewById(R.id.update_user_details_button) ;


        diseaseUpdate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedDisease=(String) adapterView.getItemAtPosition(i);
                Toast.makeText(editMyProfile.this, selectedDisease, Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bloodGroupUpdate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedBloodGroup=(String) adapterView.getItemAtPosition(i);
                Toast.makeText(editMyProfile.this, selectedBloodGroup, Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        genderUpdate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedGender=(String) adapterView.getItemAtPosition(i);
                Toast.makeText(editMyProfile.this, selectedGender, Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });





        update.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view==update)
        {
         //   FirestoreAddUserDataHelper userData =new FirestoreAddUserDataHelper() ;
           // userData.updateData(name_update.getText().toString(),address_update.getText().toString(),contactno_update.getText().toString(),Integer.parseInt(age_update.getText().toString()));
         //   Snackbar.make(view, "Data Updated Successfully", Snackbar.LENGTH_LONG)
                 //   .setAction("Action", null).show();
            showProgressDialog();
            updateUserData();


        }
    }


    private void updateUserData()
    {
        String firstName=firstNameUpdate.getText().toString().trim();
        String contactno=contactUpdate.getText().toString().trim();
        String LastName=lastNameUpdate.getText().toString().trim();
        String age=ageUpdate.getText().toString().trim();


        if(firstName.isEmpty()){
            firstNameUpdate.setError("Must be filled");
            firstNameUpdate.requestFocus();
            return;
        }
        if(LastName.isEmpty()){
            lastNameUpdate.setError("Must be filled");
            lastNameUpdate.requestFocus();
            return;
        }
        if(contactno.isEmpty()){
            contactUpdate.setError("Must be filled");
            contactUpdate.requestFocus();
            return;
        }
        if(age.isEmpty()){
            ageUpdate.setError("Must be filled");
            ageUpdate.requestFocus();
            return;
        }
        if(Integer.valueOf(age)>150|| Integer.valueOf(age) <=0)
        {
            ageUpdate.setError("Invalid Age");
        }
        finish();

        Bundle userPersonalDetials = new Bundle();
        userPersonalDetials.putString("FIRST_NAME",firstName);
        userPersonalDetials.putString("CONTACT",contactno);
        userPersonalDetials.putString("LAST_NAME",LastName);
        userPersonalDetials.putString("AGE",age);


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        userPersonalDetials.putString("GENDER",selectedGender);
        userPersonalDetials.putString("DISEASE",selectedDisease);
        userPersonalDetials.putString("BLOODGROUP",selectedBloodGroup);

        Intent intent=new Intent(editMyProfile.this,homeScreenAfterLogin.class);



        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mDatabaseReference = database.getReference();

        FirebaseAuth FirebaseAuthentication= FirebaseAuth.getInstance();
        FirebaseUser user =FirebaseAuthentication.getCurrentUser();

        FirebasePersonalDetailsEntity personalDetailsEntityObject = new FirebasePersonalDetailsEntity(userPersonalDetials);


        try {
            mDatabaseReference.child("USERS").child(user.getUid()).child("name").setValue(userPersonalDetials.getString("FIRST_NAME")+" "+userPersonalDetials.getString("LAST_NAME"));
            //   mDatabaseReference = FirebaseDatabase.getInstance().getReference();
            mDatabaseReference.child("USERS").child(user.getUid()).child("PERSONAL_DETAILS").setValue(personalDetailsEntityObject);

            Toast.makeText(this, "PROFILE UPDATED", Toast.LENGTH_SHORT).show();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Sorry There was problem updating details." +
                    "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // put some code to execute instead of forcing the application to force shutdown
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }



        startActivity(intent);





    }





    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Signing In");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
