package com.projectdeepblue.goldenhourresponse.CheckingConnectivity;

import android.content.Context;
import android.provider.Settings;

/**
 * Created by Omkar on 26-01-2018.
 */

public class ConnectionStatusGPS {

    private Context _context;

    public ConnectionStatusGPS(Context _context) {
        this._context = _context;
    }

    public boolean isGPSOn() throws Settings.SettingNotFoundException {

        int off = Settings.Secure.getInt(_context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        if(off==0){
            return false;

        }
        return true;
    }
}
