package com.projectdeepblue.goldenhourresponse;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.projectdeepblue.goldenhourresponse.FirebaseHelper.FirebasePersonalDetailsEntity;

public class myProfile extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private TextView nameTextView,contactnoTextView,ageTextView,genderTextView,bloodGroupTextView;



    private DatabaseReference myRef;
    private  ImageView  editUserProfile;
    private String nameDisplayValueFromHelperClass;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private FirebaseFirestore mFirestore;
    private FirebaseAuth mFirebaseAuth;
    private String id;

    private int flag;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        nameTextView = (TextView) findViewById(R.id.user_profile_name);
        contactnoTextView = (TextView) findViewById(R.id.contactno_TextView);
        ageTextView = (TextView) findViewById(R.id.textView_age);
        genderTextView = (TextView) findViewById(R.id.textView_gender);
        bloodGroupTextView=(TextView)findViewById(R.id.bloodGroup_TextView);
        editUserProfile = (ImageView) findViewById(R.id.editUserProfileButton);

        editUserProfile.setOnClickListener(this);

        FirebaseUser user = mFirebaseAuth.getInstance().getCurrentUser();
        id = user.getUid();
        myRef = FirebaseDatabase.getInstance().getReference().child("USERS").child(id).child("PERSONAL_DETAILS");
        try {
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    // nameTextView.setText(name);
                    FirebasePersonalDetailsEntity uInfo = dataSnapshot.getValue(FirebasePersonalDetailsEntity.class);
                    nameTextView.setText(uInfo.getFirstName().toString());

                    ageTextView.setText(uInfo.getAge().toString());
                    contactnoTextView.setText(uInfo.getContactno().toString());
                    genderTextView.setText(uInfo.getGender().toString());
                    bloodGroupTextView.setText(uInfo.getBloodGroup().toString());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Toast.makeText(myProfile.this, "ERROR LOADING DATABASE.", Toast.LENGTH_SHORT).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "ERROR LOADING DATABASE.", Toast.LENGTH_SHORT).show();
            Intent secondScreenIntetnt= new Intent(myProfile.this,homeScreenAfterLogin.class);
            startActivity(secondScreenIntetnt);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_screen_after_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_help){
            Intent helpIntent = new Intent(myProfile.this,homeScreenAfterLogin.class);
            startActivity(helpIntent);
        } else if (id == R.id.nav_my_profile) {
            Intent helpIntent = new Intent(myProfile.this,myProfile.class);
            startActivity(helpIntent);

        } else if (id == R.id.nav_relatives) {
            Intent helpIntent = new Intent(myProfile.this,relativeDataDisplay.class);
            startActivity(helpIntent);

        } else if (id == R.id.nav_medical_history) {

        } else if (id == R.id.nav_videos) {

        }   else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_about) {
            Intent helpIntent = new Intent(myProfile.this,About.class);
            startActivity(helpIntent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onClick(View view) {
        if(view== editUserProfile)
        {
            Toast.makeText(this, "Please Enter Valid Information", Toast.LENGTH_SHORT).show();
            Intent helpIntent = new Intent(myProfile.this,editMyProfile.class);
            startActivity(helpIntent);
        }
    }
}
