This git contains the file of an android app created as a solution to the problem statement given by Mastek Pvt Ltd in the competition Project Deep Blue Season 3.<br>
**Golden Hour Response-**<br>
***Problem Statement*-**<br>
A solution is required for the crucial hour after an accident has occured.
The solution needed to contain modules which helped user to get help for the victim or for himself if he is the victim of the accident.<br>
***Git files information***<br>
The file named *GoldenHourResponse* contains the files of the *android studio project*.
The solution uses cloud service Firebase as an backend and datastore.
In order to provide alerts to the victims cloud functions using FCM-(Firebase Cloud Messenger) was used.
The file named GHR-webapp and Functions contains the cloud function rules for the backend processing and the admin ui created using ReactJS.
In order to run and use the cloud function install the firebase-cli using npm.<br>
**Application Screenshots**:<br>
![](Screenshots/ss4.jpeg)<br>
![](Screenshots/ss1.jpeg)<br>
![](Screenshots/ss3.jpeg)<br>
![](Screenshots/ss2.jpeg)
